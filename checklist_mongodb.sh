SHELL=/bin/bash
PATH=/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/mongodb_4419/bin:$HOME/bin
MAILTO=root
HOME=/

export PATH

b=`hostname -s`

a=$( echo "$b" | tr -s  '[:lower:]' '[:upper:]' )
data=`date +'%d-%m-%Y'`
dt=`date +'%d%m%Y'`
dt_bkp=`date +'%Y%m%d' -d "1 day ago"`
log="/home/dba/shells/log/Checklist_Banco_Digital_"$a"_"$dt".html"

#-- identifica porta tcp
cfgfile=/etc/mongod.conf
if [ ! -f $cfgfile ]
then
   echo "[ERRO] arquivo $cfgfile nao encontrado"
   exit 2
fi

porta=`grep -A1 "^net:" $cfgfile | awk ' /port:/ {print $NF}'`
if [ -z "$porta" ]
then
   echo "[ERRO] porta tcp nao identificada"
   exit 2
fi
#-- identifica porta tcp

echo "<html><head></head><body>" > $log
echo "<table border=0 width=100%>" >> $log
echo "<tr><td  bgcolor="green"><center><font color="White" size="6">PIX - "$a" - "$data"</font></center></td></tr>"  >> $log

#-- Relatorio

accessfile=/home/dba/shells/.db.pwd
if [ ! -f $accessfile ]
then
   echo "[ERRO] arquivo $accessfile nao encontrado"
   exit 2
fi
export password=`cat $accessfile`

# -- Identificando MASTER
#master="`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/bkp.js | grep ^true$`"
#if [ ! "$master" == "true" ]
#then
#   echo "[ATENCAO] Nao sou master"
#   exit 2
#fi

version=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/ver.js | grep -Ev 'MongoDB|connecting|Implicit|bye|Error|@' | awk '{print $1 }' `

rs=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/rs.js | grep -Ev 'MongoDB|connecting|Implicit|bye|Error|@' |awk '{print $1 }' `

#members=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/members.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print "<tr><td bgcolor=lightgreen><center><font color=Black><b>" $3 "</b></font></center></td></tr>" }' |sed '1d' |sed '$d' `
member1=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/members.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '2 p' `
primary=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/members.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '3 p' `
member2=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/members.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '4 p' `
secondary=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/members.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '5 p' `
member3=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/members.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '6 p' `
secondary2=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/members.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '7 p' `
member4=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/members.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '8 p' `
secondary3=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/members.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '9 p' `
member5=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/members.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '10 p' `
secondary4=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/members.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '11 p' `
member6=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/members.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '12 p' `
secondary5=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/members.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '13p' `
member7=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/members.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '14 p' `
secondary6=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/members.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '15 p' `
member8=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/members.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '16 p' `
secondary7=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/members.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '17 p' `


#priority=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/priority.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print "<tr><td bgcolor=lightgreen><center><font color=Black><b>" $3 "</b></font></center></td></tr>" }' |sed '1d' |sed '$d' `
host1=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/priority.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '2 p' `
priority1=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/priority.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '3 p' `
host2=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/priority.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '4 p' `
priority2=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/priority.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '5 p' `
host3=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/priority.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '6 p' `
priority3=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/priority.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '7 p' `
host4=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/priority.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '8 p' `
priority4=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/priority.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '9 p' `
host5=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/priority.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '10 p' `
priority5=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/priority.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '11 p' `
host6=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/priority.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '12 p' `
priority6=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/priority.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '13 p' `
host7=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/priority.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '14 p' `
priority7=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/priority.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '15 p' `
host8=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/priority.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '16 p' `
priority8=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/priority.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|},|{|}|[|]|@|QUERY|Type' |awk  ' {print $3 }' |sed -n '17 p' `


dbs=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/dbs.js | grep -Ev 'MongoDB|connecting|Implicit|config|local|bye|Error|saving|admin|@' |awk ' {print "<tr><td bgcolor=lightgreen width=40%><left><font color=Black>Banco: <b>" $1 "</td><td bgcolor=lightgreen>Tamanho: <b>" $2 "</b></font></center></td></tr>" }'`
dbs_db=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/dbs.js | grep -Ev 'MongoDB|connecting|Implicit|config|local|bye|Error|saving|admin|@' |awk ' {print $1 }'`
dbs_tam=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/dbs.js | grep -Ev 'MongoDB|connecting|Implicit|config|local|bye|Error|saving|admin|@' |awk ' {print $2 }'`

#replica=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk  ' {print "$1" "$2" "$3" "$4 " "$5 " "$6 " "$7 " }' `

#replica=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $1" "$2" " $3" "$4" "$5" "$6" "$7}'`
source1=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $1}' |sed -n '1 p'`
source1_value=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $2}' |sed -n '1 p'`
syncedTo1=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $1}' |sed -n '2 p'`
syncedTo1_value=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $2" " $3" "$4" "$5" "$6" "$7}' |sed -n '2 p'`
deley1=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $1" "$2" " $3" "$4" "$5" "$6" "$7}' |sed -n '3 p'`
source2=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $1}' |sed -n '4 p'`
source2_value=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $2}' |sed -n '4 p'`
syncedTo2=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $1}' |sed -n '5 p'`
syncedTo2_value=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $2" " $3" "$4" "$5" "$6" "$7}' |sed -n '5 p'`
deley2=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $1" "$2" " $3" "$4" "$5" "$6" "$7}' |sed -n '6 p'`
source3=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $1}' |sed -n '7 p'`
source3_value=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $2}' |sed -n '7 p'`
syncedTo3=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $1}' |sed -n '8 p'`
syncedTo3_value=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $2" " $3" "$4" "$5" "$6" "$7}' |sed -n '8 p'`
deley3=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $1" "$2" " $3" "$4" "$5" "$6" "$7}' |sed -n '9 p'`
source4=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $1}' |sed -n '10 p'`
source4_value=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $2}' |sed -n '10 p'`
syncedTo4=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $1}' |sed -n '11 p'`
syncedTo4_value=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $2" " $3" "$4" "$5" "$6" "$7}' |sed -n '11 p'`
deley4=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $1" "$2" " $3" "$4" "$5" "$6" "$7}' |sed -n '12 p'`
#source5=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $1}' |sed -n '13 p'`
#source5_value=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $2}' |sed -n '13 p'`
#syncedTo5=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $1}' |sed -n '14 p'`
#syncedTo5_value=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $2" " $3" "$4" "$5" "$6" "$7}' |sed -n '14 p'`
#deley5=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $1" "$2" " $3" "$4" "$5" "$6" "$7}' |sed -n '15 p'`
#source6=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $1}' |sed -n '16 p'`
#source6_value=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $2}' |sed -n '16 p'`
#syncedTo6=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $1}' |sed -n '17 p'`
#syncedTo6_value=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $2" " $3" "$4" "$5" "$6" "$7}' |sed -n '17 p'`
#deley6=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/replica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}|@' |awk '{print $1" "$2" " $3" "$4" "$5" "$6" "$7}' |sed -n '18 p'`


db=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/dbs.js | grep -Ev 'MongoDB|connecting|Implicit|config|local|bye|Error|saving|admin|@' |awk ' {print $1}' > /home/dba/shells/scripts/dbs.log`

#info=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/inforeplica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}' |awk  ' {print "$1" "$2" "$3" "$4 " "$5 " "$6 " "$7 " "$8 "   "$9 "   "$10" }' `

configured=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/inforeplica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}' |awk  ' {print $1" "$2" "$3 }' |sed -n '1 p'`
configured_value=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/inforeplica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}' |awk  ' {print $4 }' |sed -n '1 p'`
log_len=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/inforeplica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}' |awk  ' {print $1" "$2" "$3" "$4" "$5 }' |sed -n '2 p'`
log_len_value=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/inforeplica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}' |awk  ' {print $6" "$7 }' |sed -n '2 p'`
oplog_first=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/inforeplica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}'|awk  ' {print $1" "$2" "$3" "$4 }'| sed -n '3 p'`
oplog_first_value=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/inforeplica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}'|awk  ' {print $5" "$6" "$7" "$8" "$9" "$10 }' |sed -n '3 p'`
oplog_last=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/inforeplica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}'|awk  ' {print $1" "$2" "$3" "$4 }' |sed -n '4 p'`
oplog_last_value=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/inforeplica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}'|awk  ' {print $5" "$6" "$7" "$8" "$9" "$10 }' |sed -n '4 p'`
now=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/inforeplica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}'|awk  ' {print $1 }' |sed -n '5 p'`
now_value=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/inforeplica.js | grep -Ev 'MongoDB|connecting|Implicit|bye|history|{|}'|awk  ' {print $2" "$3" "$4" "$5" "$6" "$7 }' |sed -n '5 p'`


#full=`cat /home/dba/shells/scripts/full_scan.js`

filename='/home/dba/shells/scripts/dbs.log'
count=`cat /home/dba/shells/scripts/dbs.log |wc -l`
while read line; do
# reading each line
echo "use $line" > "$line.js"
echo "$full" >> "$line.js"
#for i in $count; do
#       full=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/scripts/full_scan.js | grep -Ev 'MongoDB|connecting|Implicit|config|local|bye|Error|saving|admin' |awk ' {print $line}'`
#       $line=`mongo -u admin -p $password --authenticationDatabase admin --port $porta < /home/dba/shells/$line.js `
#       echo $line
#done
done < $filename

# Verifica LOG

plog=`grep "path: " $cfgfile |awk '{print $2}'`
tam_log=`du -h $plog|awk '{print $1}'`


# Coletando DF -h

/home/dba/shells/scripts/fs.sh > /home/dba/shells/scripts/df.log

INPUT="/home/dba/shells/scripts/df.log"
OLDIFS=$IFS
IFS='|'
[ ! -f $INPUT ] && { echo "$INPUT file not found"; exit 99; }



# Verifica Backup

LOG_DMP="/var/log/mongodb/backup"
ARQ="${LOG_DMP}/backup-mongo-lvm-"$dt_bkp".log"

tini="$(head -n 6 ${ARQ} |grep -Ewi "Inicio do backup..." |awk '{ print $1" "$2" "$3" "$4" "$5" "$6}')"
tfim="$(tail -n 2 ${ARQ} |grep -Ewi "Fim do backup.|ERROR" |awk '{ print $1" "$2" "$3" "$4" "$5" "$6 " "$7 " "$8 " " $9 " " $10}')"

v_1=`cat ${ARQ} |grep -ws "Tamanho do backup..." -A 1|awk '{print $1" " $2}' > /home/dba/shells/tam_bkp.txt`
file_bkp=`tail -n 1 /home/dba/shells/tam_bkp.txt |awk '{print $2}'`
tam_bkp=`tail -n 1 /home/dba/shells/tam_bkp.txt |awk '{print $1}'`


# Versao
#echo "<tr><td  bgcolor="green"><center><font color="White" size="5"><b>VERSAO MONGODB</b></font></center></td></tr>"  >> $log
echo "<table width=100%>" >> $log
echo "<tr><td  bgcolor="green" colspan="2"><center><font color="White" size="5"><b>VERSAO MONGODB</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40% colspan="2""><center><font color=Black><b>$version</b></font></center></td></tr>"  >> $log
echo "</table>" >> $log

# ReplicaSet
#echo "<tr><td  bgcolor="green"><center><font color="White" size="5"><b>Nome ReplicaSet</b></font></center></td></tr>"  >> $log
echo "<table width=100%>" >> $log
echo "<tr><td  bgcolor="green" colspan="2"><center><font color="White" size="5"><b>Nome ReplicaSet</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" colspan="2"><center><font color=Black><b>$rs</b></font></center></td></tr>"  >> $log
echo "</table>" >> $log

# Members
#echo "<tr><td  bgcolor="green"><center><font color="White" size="5"><b>Membros e Role</b></font></center></td></tr>"  >> $log
#echo "$members"  >> $log
echo "<table width=100%>" >> $log
echo "<tr><td  bgcolor="green" colspan="2"><center><font color="White" size="5"><b>Role e Membros</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>$primary</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$member1"</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>$secondary</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$member2"</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>$secondary2</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$member3"</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>$secondary3</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$member4"</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>$secondary4</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$member5"</b></font></center></td></tr>"  >> $log
#echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>$secondary5</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$member6"</b></font></center></td></tr>"  >> $log
#echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>$secondary6</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$member7"</b></font></center></td></tr>"  >> $log
#echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>$secondary7</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$member8"</b></font></center></td></tr>"  >> $log
echo "</table>" >> $log


# Priority
#echo "<tr><td  bgcolor="green"><center><font color="White" size="5"><b>Priority</b></font></center></td></tr>"  >> $log
#echo "<td  bgcolor="green"><center><font color="White" size="5"><b>Priority</b></font></center></td></tr>"  >> $log
#echo "$priority"  >> $log
echo "<table width=100%>" >> $log
echo "<td  bgcolor="green" colspan="2"><center><font color="White" size="5"><b>Priority</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>Servidor</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$host1"</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>Prioridade</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$priority1"</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>Servidor</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$host2"</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>Prioridade</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$priority2"</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>Servidor</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$host3"</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>Prioridade</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$priority3"</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>Servidor</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$host4"</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>Prioridade</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$priority4"</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>Servidor</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$host5"</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>Prioridade</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$priority5"</b></font></center></td></tr>"  >> $log
#echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>Servidor</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$host6"</b></font></center></td></tr>"  >> $log
#echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>Prioridade</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$priority6"</b></font></center></td></tr>"  >> $log
#echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>Servidor</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$host7"</b></font></center></td></tr>"  >> $log
#echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>Prioridade</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$priority7"</b></font></center></td></tr>"  >> $log
#echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>Servidor</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$host8"</b></font></center></td></tr>"  >> $log
#echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>Prioridade</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$priority8"</b></font></center></td></tr>"  >> $log
echo "</table>" >> $log


# Info Replica
#echo "<tr><td  bgcolor="green" colspan="2"><center><font color="White" size="5"><b>Info Replica</b></font></center></td></tr>"  >> $log
#echo "<b>$info</b>"  >> $log

echo "<table width=100%>" >> $log
echo "<tr><td  bgcolor="green" colspan="2"><center><font color="White" size="5"><b>Info Replica</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>"$configured "</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$configured_value"</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>"$log_len"</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$log_len_value"</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>"$oplog_first"</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$oplog_first_value"</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>"$oplog_last"</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$oplog_last_value"</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>"$now"</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$now_value"</b></font></center></td></tr>"  >> $log
echo "</table>" >> $log


# Replica
#echo "<tr><td bgcolor="green"><center><font color="White" size="5"><b>Status Replica</b></font></center></td></tr>"  >> $log
#echo "<tr><td bgcolor=lightgreen><center><font color=Black><b>"$replica"</b></font></center></td></tr>" >> $log

echo "<table width=100%>" >> $log
echo "<tr><td bgcolor="green" colspan="2"><center><font color="White" size="5"><b>Status Replica</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>"$source1"</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$source1_value"</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>"$syncedTo1"</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$syncedTo1_value"</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b></b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$deley1"</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>"$source2"</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$source2_value"</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>"$syncedTo2"</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$syncedTo2_value"</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b></b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$deley2"</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>"$source3"</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$source3_value"</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>"$syncedTo3"</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$syncedTo3_value"</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b></b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$deley3"</b></font></center></td></tr>"  >> $log
#echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>"$source4"</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$source4_value"</b></font></center></td></tr>"  >> $log
#echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>"$syncedTo4"</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$syncedTo4_value"</b></font></center></td></tr>"  >> $log
#echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b></b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$deley4"</b></font></center></td></tr>"  >> $log
#echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>"$source5"</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$source5_value"</b></font></center></td></tr>"  >> $log
#echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>"$syncedTo5"</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$syncedTo5_value"</b></font></center></td></tr>"  >> $log
#echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b></b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$deley5"</b></font></center></td></tr>"  >> $log
#echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>"$source6"</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$source6_value"</b></font></center></td></tr>"  >> $log
#echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b>"$syncedTo6"</b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$syncedTo6_value"</b></font></center></td></tr>"  >> $log
#echo "<tr><td bgcolor=lightgreen width="40%" ><center><font color=Black><b></b></font></center></td><td bgcolor=lightgreen><center><font color=Black><b>"$deley6"</b></font></center></td></tr>"  >> $log
echo "</table>" >> $log


# BACKUP

#echo "<tr><td  bgcolor="green" colspan="2"><center><font color="White" size="5"><b>Backup MongoDB</b></font></center></td></tr>"  >> $log

echo "<table width=100%>" >> $log
echo "<tr><td  bgcolor="green" colspan="2"><center><font color="White" size="5"><b>Backup MongoDB</b></font></center></td></tr>"  >> $log
echo "<tr><td  bgcolor="lightgreen" width="40%" ><center><font color="black"><b>Inicio:</b></font></center></td><td  bgcolor="lightgreen"><center><font color="black"><b>$tini</b></font></center></td></tr>"  >> $log
echo "<tr><td  bgcolor="lightgreen" width="40%" ><center><font color="black"><b>Fim:</b></font></center></td><td  bgcolor="lightgreen"><center><font color="black"><b>$tfim</b></font></center></td></tr>"  >> $log
echo "<tr><td  bgcolor="lightgreen" width="40%" ><center><font color="black"><b>Caminho Backup:</b></font></center></td><td  bgcolor="lightgreen"><center><font color="black"><b>$file_bkp</b></font></center></td></tr>"  >> $log
echo "<tr><td  bgcolor="lightgreen" width="40%" ><center><font color="black"><b>Tamanho:</b></font></center></td><td  bgcolor="lightgreen"><center><font color="black"><b>$tam_bkp</b></font></center></td></tr>"  >> $log
echo "</table>" >> $log


# Databases
#echo "<tr><td  bgcolor="green"><center><font color="White" size="5"><b>DATABASES</b></font></center></td></tr>"  >> $log
#echo "<table width=100%>" >> $log
#echo "<tr><td>" >> $log
echo "<table width=100%>" >> $log
echo "<tr><td  bgcolor="green" colspan="2"><center><font color="White" size="5"><b>DATABASES</b></font></center></td></tr>"  >> $log
echo "$dbs" >> $log
echo "</table>" >> $log

# LOG
#echo "<tr><td  bgcolor="green"><center><font color="White" size="5"><b>Log</b></font></center></td></tr>"  >> $log
echo "<table width=100%>" >> $log
echo "<tr><td  bgcolor="green" colspan="2"><center><font color="White" size="5"><b>Log</b></font></center></td></tr>"  >> $log
echo "<tr><td  bgcolor="lightgreen" width="40%" ><center><font color="black"><b>Caminho:</b></font></center></td><td  bgcolor="lightgreen"><center><font color="black"><b>$plog</b></font></center></td>"  >> $log
echo "<tr><td  bgcolor="lightgreen" width="40%" ><center><font color="black"><b>Tamanho:</b></font></center></td><td  bgcolor="lightgreen"><center><font color="black"><b>$tam_log</b></font></center></td></tr>"  >> $log
echo "</table>" >> $log

# FileSystem - DF
echo "<table border=0   width="100%">" >> $log
echo "<tr><td  bgcolor="green" colspan="6"><center><font color="White" size="5"><b>FileSystem</b></font></center></td></tr>"  >> $log
echo "<tr><td bgcolor=green><center><font color=White><b>Mount</b></center></td>" >> $log
echo "<td bgcolor=green><center><font color=White><b>Total Size</b></center></td>" >> $log
echo "<td bgcolor=green><center><font color=White><b>Total Use</b></center></td>" >> $log
echo "<td bgcolor=green><center><font color=White><b>Total Available</b></center></td>" >> $log
echo "<td bgcolor=green><center><font color=White><b>Percent Use %</b></center></td></tr>" >> $log

while read fs sz us av pr vg
do

echo "<tr><td  bgcolor="lightgreen"><font color="black" align="left"><b>$fs</font></b></td>"  >> $log
echo "<td  bgcolor="lightgreen"><center><font color="black"><b>$sz</b></font></center></td>"  >> $log
echo "<td  bgcolor="lightgreen"><center><font color="black"><b>$us</b></font></center></td>"  >> $log
echo "<td  bgcolor="lightgreen"><center><font color="black"><b>$av</b></font></center></td>"  >> $log
echo "<td  bgcolor="lightgreen"><center><font color="black"><b>$pr</b></font></center></td></tr>"  >> $log

done < $INPUT
echo "</table>" >> $log


echo "</table>" >> $log
echo "</body></html>" >> $log

#
# Envia aviso via e-mail
#

  ASSUNTO="Checklist MongoDB - PIX"
  #MAIL_AVISO="sergio.queiroz@modal.com.br"
  MAIL_AVISO="dba@modal.com.br"
  #MAIL_AVISO="rafael.silva@modal.com.br"

 #mailx -a $log -s "$ASSUNTO" $MAIL_AVISO < /dev/null

export MAILTO=$MAIL_AVISO
export CONTENT=$log
export SUBJECT=$ASSUNTO
(
 echo "Subject: $SUBJECT"
 echo "MIME-Version: 1.0"
 echo "Content-Type: text/html"
 echo "Content-Disposition: inline"
 cat $CONTENT
)| sendmail $MAILTO

sleep 5

gzip -f $log


