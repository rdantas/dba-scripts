USE [msdb]
GO

/****** Object:  Job [AlwaysOn_Latency_Data_Collection]    Script Date: 10/6/2023 9:29:03 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/6/2023 9:29:03 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'AlwaysOn_Latency_Data_Collection', 
		@enabled=0, 
		@notify_level_eventlog=2, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Collect AG Information]    Script Date: 10/6/2023 9:29:03 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Collect AG Information', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N' USE tempdb
                  IF OBJECT_ID(''AGInfo'') IS NOT NULL
                      BEGIN
                        DROP TABLE AGInfo
                   END 
                  IF OBJECT_ID(''LatencyCollectionStatus'') IS NOT NULL
                      BEGIN
                        DROP TABLE LatencyCollectionStatus
                      END
                   CREATE TABLE LatencyCollectionStatus(
                        [collection_status] [NVARCHAR](60)  NULL,
                        [start_timestamp] [DATETIMEOFFSET] NULL,
                        [startutc_timestamp] [DATETIMEOFFSET] NULL
                    )
                  INSERT INTO LatencyCollectionStatus(collection_status, start_timestamp, startutc_timestamp) values (''Started'', GETDATE(), GETUTCDATE())
                  SELECT
                  AGC.name as agname
                  , RCS.replica_server_name as replica_name
                  , ARS.role_desc as agrole
                  INTO AGInfo
                  FROM
                      sys.availability_groups_cluster AS AGC
                      INNER JOIN sys.dm_hadr_availability_replica_cluster_states AS RCS
                      ON
                      RCS.group_id = AGC.group_id
                      INNER JOIN sys.dm_hadr_availability_replica_states AS ARS
                      ON
                      ARS.replica_id = RCS.replica_id
                      where AGC.name =  N''AG_05''', 
		@database_name=N'tempdb', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Create XE Session]    Script Date: 10/6/2023 9:29:03 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Create XE Session', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'IF EXISTS (select * from sys.server_event_sessions 
                WHERE name = N''AlwaysOn_Data_Movement_Tracing'')
                    BEGIN
                    DROP EVENT SESSION [AlwaysOn_Data_Movement_Tracing] ON SERVER 
                    END
                CREATE EVENT SESSION [AlwaysOn_Data_Movement_Tracing] ON SERVER ADD EVENT sqlserver.hadr_apply_log_block, 
ADD EVENT sqlserver.hadr_capture_log_block, 
ADD EVENT sqlserver.hadr_database_flow_control_action, 
ADD EVENT sqlserver.hadr_db_commit_mgr_harden, 
ADD EVENT sqlserver.hadr_log_block_send_complete, 
ADD EVENT sqlserver.hadr_send_harden_lsn_message, 
ADD EVENT sqlserver.hadr_transport_flow_control_action, 
ADD EVENT sqlserver.log_flush_complete, 
ADD EVENT sqlserver.log_flush_start, 
ADD EVENT sqlserver.recovery_unit_harden_log_timestamps, 
ADD EVENT sqlserver.log_block_pushed_to_logpool, 
ADD EVENT sqlserver.hadr_transport_receive_log_block_message, 
ADD EVENT sqlserver.hadr_receive_harden_lsn_message, 
ADD EVENT sqlserver.hadr_log_block_group_commit, 
ADD EVENT sqlserver.hadr_log_block_compression, 
ADD EVENT sqlserver.hadr_log_block_decompression, 
ADD EVENT sqlserver.hadr_lsn_send_complete, 
ADD EVENT sqlserver.hadr_capture_filestream_wait, 
ADD EVENT sqlserver.hadr_capture_vlfheader ADD TARGET package0.event_file(SET filename=N''AlwaysOn_Data_Movement_Tracing.xel'',max_file_size=(25),max_rollover_files=(4))
                WITH (MAX_MEMORY=4096 KB,EVENT_RETENTION_MODE=ALLOW_SINGLE_EVENT_LOSS,MAX_DISPATCH_LATENCY=30 SECONDS,MAX_EVENT_SIZE=0 KB,MEMORY_PARTITION_MODE=NONE,TRACK_CAUSALITY=OFF,STARTUP_STATE=ON)
                
                ALTER EVENT SESSION [AlwaysOn_Data_Movement_Tracing] ON SERVER STATE = START', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Wait For Collection]    Script Date: 10/6/2023 9:29:03 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Wait For Collection', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'WAITFOR DELAY ''00:2:00'' 
                                                       GO', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [End XE Session]    Script Date: 10/6/2023 9:29:03 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'End XE Session', 
		@step_id=4, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'ALTER EVENT SESSION [AlwaysOn_Data_Movement_Tracing] ON SERVER STATE = STOP', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Extract XE Data]    Script Date: 10/6/2023 9:29:03 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Extract XE Data', 
		@step_id=5, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'
                    BEGIN TRANSACTION
                    USE tempdb
                    IF OBJECT_ID(''#EventXml'') IS NOT NULL
                    BEGIN
                        DROP TABLE #EventXml
                    END 

                    SELECT 
                        xe.event_name, 
                        CAST(xe.event_data AS XML) AS event_data
                    INTO #EventXml
                    FROM
                    (
                    SELECT
                            object_name AS event_name,
                            CAST(event_data AS XML) AS event_data
                        FROM sys.fn_xe_file_target_read_file(
                                    ''AlwaysOn_Data_Movement_Tracing*.xel'', 
                                    NULL, NULL, NULL)
                        WHERE object_name IN (''hadr_log_block_group_commit'',
                                    ''log_block_pushed_to_logpool'',
                                    ''log_flush_start'',
                                    ''log_flush_complete'',
                                    ''hadr_log_block_compression'',
                                    ''hadr_capture_log_block'',
                                    ''hadr_capture_filestream_wait'',
                                    ''hadr_log_block_send_complete'',
                                    ''hadr_receive_harden_lsn_message'',
                                    ''hadr_db_commit_mgr_harden'',
                                    ''recovery_unit_harden_log_timestamps'',
                                    ''hadr_capture_vlfheader'',
                                    ''hadr_log_block_decompression'',
                                    ''hadr_apply_log_block'',
                                    ''hadr_send_harden_lsn_message'',
                                    ''hadr_log_block_decompression'',
                                    ''hadr_lsn_send_complete'',
                                    ''hadr_transport_receive_log_block_message'')
    
                    ) xe

                    IF OBJECT_ID(''DMReplicaEvents'') IS NOT NULL
                    BEGIN
                        DROP TABLE DMReplicaEvents
                    END 

                    SET ANSI_NULLS ON

                    SET QUOTED_IDENTIFIER ON

                    CREATE TABLE DMReplicaEvents(
                        [server_name] [NVARCHAR](128) NULL,
                        [event_name] [NVARCHAR](60) NOT NULL,
                        [log_block_id] [BIGINT] NULL,
                        [database_id] [INT] NULL,
                        [processing_time] [BIGINT] NULL,
                        [start_timestamp] [BIGINT] NULL,
                        [publish_timestamp] [DATETIMEOFFSET] NULL,
                        [log_block_size] [BIGINT] NULL,
                        [target_availability_replica_id] [UNIQUEIDENTIFIER] NULL,
                        [local_availability_replica_id] [UNIQUEIDENTIFIER] NULL,
                        [database_replica_id] [UNIQUEIDENTIFIER] NULL,
                        [mode] [BIGINT] NULL,
                        [availability_group_id] [UNIQUEIDENTIFIER] NULL,
                        [pending_writes]  [BIGINT] NULL
                    )

                    IF OBJECT_ID(''LatencyResults'') IS NOT NULL
                    BEGIN
                        DROP TABLE LatencyResults
                    END 
                    CREATE TABLE LatencyResults(
                       [event_name] [NVARCHAR](60) NOT NULL,
                       [processing_time] [BIGINT] NULL,
                       [publish_timestamp] [DATETIMEOFFSET] NULL,
                       [server_commit_mode] [NVARCHAR](60) NULL
                    )


                    INSERT INTO DMReplicaEvents
                    SELECT 
                        @@SERVERNAME AS server_name,
                        xe.event_name,
                        AoData.value(''(data[@name="log_block_id"]/value)[1]'', ''BIGINT'') AS log_block_id,
                        NULL AS database_id,
                        AoData.value(''(data[@name="total_processing_time"]/value)[1]'', ''BIGINT'') AS processing_time,
                        AoData.value(''(data[@name="start_timestamp"]/value)[1]'', ''BIGINT'') AS start_timestamp,
                        CAST(SUBSTRING(CAST(xe.event_data AS NVARCHAR(MAX)), 75, 24) AS DATETIMEOFFSET) AS publish_timestamp,
                        AoData.value(''(data[@name="log_block_size"]/value)[1]'', ''BIGINT'') AS log_block_size,
                        NULL AS target_availability_replica_id,
                        NULL AS local_availability_replica_id,
                        NULL AS database_replica_id,
                        NULL AS mode,
                        NULL AS availability_group_id,
                        NULL AS pending_writes
                    FROM #EventXml AS xe
                    CROSS APPLY xe.event_data.nodes(''/event'')  AS T(AoData)
                    WHERE xe.event_name = ''hadr_log_block_send_complete''

                    GO


                    INSERT INTO DMReplicaEvents
                    SELECT 
                        @@SERVERNAME AS server_name,
                        xe.event_name,
                        AoData.value(''(data[@name="log_block_id"]/value)[1]'', ''BIGINT'') AS log_block_id,
                        AoData.value(''(data[@name="database_id"]/value)[1]'', ''INT'') AS database_id,
                        AoData.value(''(data[@name="duration"]/value)[1]'', ''BIGINT'') AS processing_time,
                        AoData.value(''(data[@name="start_timestamp"]/value)[1]'', ''BIGINT'') AS start_timestamp,
                        CAST(SUBSTRING(CAST(xe.event_data AS NVARCHAR(MAX)), 65, 24) AS DATETIMEOFFSET) AS publish_timestamp,
                        NULL AS log_block_size,
                        NULL AS target_availability_replica_id,
                        NULL AS local_availability_replica_id,
                        NULL AS database_replica_id,
                        NULL AS mode,
                        NULL AS availability_group_id,
                        AoData.value(''(data[@name="pending_writes"]/value)[1]'',''BIGINT'') AS pending_writes
                    FROM #EventXml AS xe
                    CROSS APPLY xe.event_data.nodes(''/event'')  AS T(AoData)
                    WHERE xe.event_name = ''log_flush_complete''

                    GO

                    INSERT INTO DMReplicaEvents
                    SELECT 
                        @@SERVERNAME AS server_name,
                        xe.event_name,
                        NULL AS log_block_id,
                        AoData.value(''(data[@name="database_id"]/value)[1]'', ''BIGINT'') AS database_id,
                        AoData.value(''(data[@name="time_to_commit"]/value)[1]'', ''BIGINT'') AS processing_time,
                        NULL AS start_timestamp,
                        CAST(SUBSTRING(CAST(xe.event_data AS NVARCHAR(MAX)), 72, 24) AS DATETIMEOFFSET) AS publish_timestamp,
                        NULL AS log_block_size,
                        AoData.value(''(data[@name="replica_id"]/value)[1]'', ''UNIQUEIDENTIFIER'') AS target_availability_replica_id,
                        NULL AS local_availability_replica_id,
                        AoData.value(''(data[@name="ag_database_id"]/value)[1]'', ''UNIQUEIDENTIFIER'') AS database_replica_id,
                        NULL AS mode,
                        AoData.value(''(data[@name="group_id"]/value)[1]'',''UNIQUEIDENTIFIER'') AS availability_group_id,
                        NULL AS pending_writes
                    FROM #EventXml AS xe
                    CROSS APPLY xe.event_data.nodes(''/event'')  AS T(AoData)
                    WHERE xe.event_name = ''hadr_db_commit_mgr_harden''

                    GO


                    INSERT INTO DMReplicaEvents
                    SELECT 
                        @@SERVERNAME AS server_name,
                        xe.event_name,
                        AoData.value(''(data[@name="log_block_id"]/value)[1]'', ''BIGINT'') AS log_block_id,
                        AoData.value(''(data[@name="database_id"]/value)[1]'', ''BIGINT'') AS database_id,
                        AoData.value(''(data[@name="processing_time"]/value)[1]'', ''BIGINT'') AS processing_time,
                        AoData.value(''(data[@name="start_timestamp"]/value)[1]'', ''BIGINT'') AS start_timestamp,
                        CAST(SUBSTRING(CAST(xe.event_data AS NVARCHAR(MAX)), 82, 24) AS DATETIMEOFFSET) AS publish_timestamp,
                        NULL AS log_block_size,
                        NULL AS target_availability_replica_id,
                        NULL AS local_availability_replica_id,
                        NULL AS database_replica_id,
                        NULL AS mode,
                        NULL AS availability_group_id,
                        NULL AS pending_writes
                    FROM #EventXml AS xe
                    CROSS APPLY xe.event_data.nodes(''/event'')  AS T(AoData)
                    WHERE xe.event_name = ''recovery_unit_harden_log_timestamps''

                    GO

                    INSERT INTO DMReplicaEvents
                    SELECT 
                        @@SERVERNAME AS server_name,
                        xe.event_name,
                        AoData.value(''(data[@name="log_block_id"]/value)[1]'', ''BIGINT'') AS log_block_id,
                        AoData.value(''(data[@name="database_id"]/value)[1]'', ''BIGINT'') AS database_id,
                        AoData.value(''(data[@name="processing_time"]/value)[1]'', ''BIGINT'') AS processing_time,
                        AoData.value(''(data[@name="start_timestamp"]/value)[1]'', ''BIGINT'') AS start_timestamp,
                        CAST(SUBSTRING(CAST(xe.event_data AS NVARCHAR(MAX)), 73, 24) AS DATETIMEOFFSET) AS publish_timestamp,
                        AoData.value(''(data[@name="uncompressed_size"]/value)[1]'', ''INT'') AS log_block_size,
                        AoData.value(''(data[@name="availability_replica_id"]/value)[1]'', ''UNIQUEIDENTIFIER'') AS target_availability_replica_id,
                        NULL AS local_availability_replica_id,
                        NULL AS database_replica_id,
                        NULL AS mode,
                        NULL AS availability_group_id,
                        NULL AS pending_writes
                    FROM #EventXml AS xe
                    CROSS APPLY xe.event_data.nodes(''/event'')  AS T(AoData)
                    WHERE xe.event_name = ''hadr_log_block_compression''

                    GO


                    INSERT INTO DMReplicaEvents
                    SELECT 
                        @@SERVERNAME AS server_name,
                        xe.event_name,
                        AoData.value(''(data[@name="log_block_id"]/value)[1]'', ''BIGINT'') AS log_block_id,
                        AoData.value(''(data[@name="database_id"]/value)[1]'', ''BIGINT'') AS database_id,
                        AoData.value(''(data[@name="processing_time"]/value)[1]'', ''BIGINT'') AS processing_time,
                        AoData.value(''(data[@name="start_timestamp"]/value)[1]'', ''BIGINT'') AS start_timestamp,
                        CAST(SUBSTRING(CAST(xe.event_data AS NVARCHAR(MAX)), 75, 24) AS DATETIMEOFFSET) AS publish_timestamp,
                        AoData.value(''(data[@name="uncompressed_size"]/value)[1]'', ''BIGINT'') AS log_block_size,
                        AoData.value(''(data[@name="availability_replica_id"]/value)[1]'', ''UNIQUEIDENTIFIER'') AS target_availability_replica_id,
                        NULL AS local_availability_replica_id,
                        NULL AS database_replica_id,
                        NULL AS mode,
                        NULL AS availability_group_id,
                        NULL AS pending_writes
                    FROM #EventXml AS xe
                    CROSS APPLY xe.event_data.nodes(''/event'')  AS T(AoData)
                    WHERE xe.event_name = ''hadr_log_block_decompression''

                    INSERT INTO DMReplicaEvents
                    SELECT 
                        @@SERVERNAME AS server_name,
                        xe.event_name,
                        AoData.value(''(data[@name="log_block_id"]/value)[1]'', ''BIGINT'') AS log_block_id,
                        NULL AS database_id,
                        AoData.value(''(data[@name="total_sending_time"]/value)[1]'', ''BIGINT'') AS processing_time,
                        AoData.value(''(data[@name="start_timestamp"]/value)[1]'', ''BIGINT'') AS start_timestamp,
                        CAST(SUBSTRING(CAST(xe.event_data AS NVARCHAR(MAX)), 69, 24) AS DATETIMEOFFSET) AS publish_timestamp,
                        NULL AS log_block_size,
                        NULL AS target_availability_replica_id,
                        NULL AS local_availability_replica_id,
                        NULL AS database_replica_id,
                        NULL AS mode,
                        NULL AS availability_group_id,
                        NULL AS pending_writes
                    FROM #EventXml AS xe
                    CROSS APPLY xe.event_data.nodes(''/event'')  AS T(AoData)
                    WHERE xe.event_name = ''hadr_lsn_send_complete''

                    INSERT INTO DMReplicaEvents
                    SELECT 
                        @@SERVERNAME AS server_name,
                        xe.event_name,
                        AoData.value(''(data[@name="log_block_id"]/value)[1]'', ''BIGINT'') AS log_block_id,
                        NULL AS database_id,
                        AoData.value(''(data[@name="processing_time"]/value)[1]'', ''BIGINT'') AS processing_time,
                        AoData.value(''(data[@name="start_timestamp"]/value)[1]'', ''BIGINT'') AS start_timestamp,
                        CAST(SUBSTRING(CAST(xe.event_data AS NVARCHAR(MAX)), 87, 24) AS DATETIMEOFFSET) AS publish_timestamp,
                        NULL AS log_block_size,
                        AoData.value(''(data[@name="target_availability_replica_id"]/value)[1]'', ''UNIQUEIDENTIFIER'') AS target_availability_replica_id,
                        AoData.value(''(data[@name="local_availability_replica_id"]/value)[1]'', ''UNIQUEIDENTIFIER'') AS local_availability_replica_id,
                        AoData.value(''(data[@name="target_availability_replica_id"]/value)[1]'', ''UNIQUEIDENTIFIER'') AS database_replica_id,
                        AoData.value(''(data[@name="mode"]/value)[1]'', ''BIGINT'') AS mode,
                        AoData.value(''(data[@name="availability_group_id"]/value)[1]'',''UNIQUEIDENTIFIER'') AS availability_group_id,
                        NULL AS pending_writes
                    FROM #EventXml AS xe
                    CROSS APPLY xe.event_data.nodes(''/event'')  AS T(AoData)
                    WHERE xe.event_name = ''hadr_transport_receive_log_block_message''


                    DELETE
                    FROM DMReplicaEvents
                    WHERE CAST(publish_timestamp AS DATETIME) < DATEADD(minute, -2, CAST((SELECT MAX(publish_timestamp) from DMReplicaEvents) as DATETIME))
                    COMMIT
                    GO', 
		@database_name=N'tempdb', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Create Result Set]    Script Date: 10/6/2023 9:29:03 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Create Result Set', 
		@step_id=6, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'
                    BEGIN TRANSACTION
                    USE tempdb
                    declare @ag_id as nvarchar(60) 
                    declare @event as nvarchar(60) 
                    set @ag_id = (select group_id from  sys.availability_groups_cluster where name = N''AG_05'')
                    IF OBJECT_ID(''DbIdTable'') IS NOT NULL
                    BEGIN
                        DROP TABLE DbIdTable
                    END 
                    CREATE TABLE DbIdTable(
                        [database_id] [INT] NULL
                    )

                    INSERT INTO DbIdTable
                    select distinct database_id  from sys.dm_hadr_database_replica_states where group_id=@ag_id 

                    delete from tempdb.dbo.DMReplicaEvents where not (availability_group_id = @ag_id or availability_group_id is NULL) 

                    delete from tempdb.dbo.DMReplicaEvents where not (database_id in (select database_id from DbIdTable) or database_id is NULL)

                    set @event = ''availability_mode_desc''
                    INSERT INTO LatencyResults
                    select @event, NULL as processing_time, NULL as publish_timestamp, availability_mode_desc as server_commit_mode from sys.availability_replicas  A
                    inner join 
                    (select * from sys.dm_hadr_availability_replica_states) B
                    on A.replica_id = B.replica_id and A.group_id = @ag_id and A.replica_server_name = @@SERVERNAME

                    set @event = ''start_time''
                    INSERT INTO LatencyResults
                    select @event as event_name, NULL as processing_time, min(publish_timestamp) as publish_timestamp, NULL as server_commit_mode from tempdb.dbo.DMReplicaEvents

                    set @event = ''recovery_unit_harden_log_timestamps''
                    INSERT INTO LatencyResults
                    select @event, avg(processing_time), min(publish_timestamp) as publish_timestamp, NULL as server_commit_mode from DMReplicaEvents where event_name=''recovery_unit_harden_log_timestamps'' GROUP BY DATEPART(YEAR, publish_timestamp), DATEPART(MONTH, publish_timestamp), DATEPART(DAY, publish_timestamp), DATEPART(HOUR, publish_timestamp), DATEPART(MINUTE, publish_timestamp), DATEPART(SECOND, publish_timestamp) 

                    set @event = ''avg_recovery_unit_harden_log_timestamps''
                    INSERT INTO LatencyResults
                    select @event as event_name,AVG(processing_time) as processing_time, NULL as publish_timestamp, NULL as server_commit_mode from tempdb.dbo.DMReplicaEvents where event_name=''recovery_unit_harden_log_timestamps'' 

                    set @event = ''hadr_db_commit_mgr_harden''
                    INSERT INTO LatencyResults
                    select @event, avg(processing_time), min(publish_timestamp) as publish_timestamp, NULL as server_commit_mode from DMReplicaEvents where event_name=''hadr_db_commit_mgr_harden'' GROUP BY DATEPART(YEAR, publish_timestamp), DATEPART(MONTH, publish_timestamp), DATEPART(DAY, publish_timestamp), DATEPART(HOUR, publish_timestamp), DATEPART(MINUTE, publish_timestamp), DATEPART(SECOND, publish_timestamp)

                    set @event = ''avg_hadr_db_commit_mgr_harden''
                    INSERT INTO LatencyResults
                    SELECT @event as event_name, AVG(processing_time) as processing_time, NULL as publish_timestamp, NULL as server_commit_mode from tempdb.dbo.DMReplicaEvents where event_name=''hadr_db_commit_mgr_harden''

                    set @event = ''avg_hadr_log_block_send_complete''
                    INSERT INTO LatencyResults
                    SELECT @event as event_name, AVG(processing_time) as processing_time, NULL as publish_timestamp, NULL as server_commit_mode FROM tempdb.dbo.DMReplicaEvents WHERE event_name = ''hadr_log_block_send_complete''

                    set @event = ''avg_hadr_log_block_compression''
                    INSERT INTO LatencyResults
                    SELECT @event as event_name, AVG(processing_time) as processing_time, NULL as publish_timestamp, NULL as server_commit_mode from tempdb.dbo.DMReplicaEvents where event_name=''hadr_log_block_compression''

                    set @event = ''avg_hadr_log_block_decompression''
                    INSERT INTO LatencyResults
                    select @event as event_name, AVG(processing_time) as processing_time, NULL as publish_timestamp, NULL as server_commit_mode from tempdb.dbo.DMReplicaEvents where event_name=''hadr_log_block_decompression''

                    set @event = ''hadr_lsn_send_complete''
                    INSERT INTO LatencyResults
                    select @event, avg(processing_time), min(publish_timestamp) as publish_timestamp, NULL as server_commit_mode from DMReplicaEvents where event_name=''hadr_lsn_send_complete'' GROUP BY DATEPART(YEAR, publish_timestamp), DATEPART(MONTH, publish_timestamp), DATEPART(DAY, publish_timestamp), DATEPART(HOUR, publish_timestamp), DATEPART(MINUTE, publish_timestamp), DATEPART(SECOND, publish_timestamp) 

                    set @event = ''avg_hadr_lsn_send_complete''
                    INSERT INTO LatencyResults
                    select @event as event_name, AVG(processing_time) as processing_time, NULL as publish_timestamp, NULL as server_commit_mode from tempdb.dbo.DMReplicaEvents where event_name=''hadr_lsn_send_complete''

                    set @event = ''avg_hadr_transport_receive_log_block_message''
                    INSERT INTO LatencyResults
                    select @event as event_name, AVG(processing_time) as processing_time, NULL as publish_timestamp, NULL as server_commit_mode from tempdb.dbo.DMReplicaEvents where event_name=''hadr_transport_receive_log_block_message''


                    set @event = ''avg_log_flush_complete''
                    INSERT INTO LatencyResults
                    select @event as event_name, AVG(processing_time*1000) as processing_time, NULL as publish_timestamp, NULL as server_commit_mode from tempdb.dbo.DMReplicaEvents where event_name=''log_flush_complete''
                    COMMIT

            ', 
		@database_name=N'tempdb', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Drop XE Session]    Script Date: 10/6/2023 9:29:03 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Drop XE Session', 
		@step_id=7, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DROP EVENT SESSION [AlwaysOn_Data_Movement_Tracing] ON SERVER
                                                            UPDATE tempdb.dbo.LatencyCollectionStatus set collection_status =''Completed''', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [cdc.DBSMART_CTA_capture]    Script Date: 10/6/2023 9:29:03 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [REPL-LogReader]    Script Date: 10/6/2023 9:29:03 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'REPL-LogReader' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'REPL-LogReader'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'cdc.DBSMART_CTA_capture', 
		@enabled=1, 
		@notify_level_eventlog=2, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'CDC Log Scan Job', 
		@category_name=N'REPL-LogReader', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [[1] Check Primary]    Script Date: 10/6/2023 9:29:03 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'[1] Check Primary', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @check INT

SELECT @check = sys.fn_hadr_is_primary_replica ( ''DBSMART_CTA'' )

IF @check = 0 
 raiserror (''This is not the primary replica.'',2,1)', 
		@database_name=N'master', 
		@flags=4
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Starting Change Data Capture Collection Agent]    Script Date: 10/6/2023 9:29:03 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Starting Change Data Capture Collection Agent', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=10, 
		@retry_interval=1, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'RAISERROR(22801, 10, -1)', 
		@server=N'SQLPRD01-OSA', 
		@database_name=N'DBSMART_CTA', 
		@flags=4
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Change Data Capture Collection Agent]    Script Date: 10/6/2023 9:29:03 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Change Data Capture Collection Agent', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=10, 
		@retry_interval=1, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'sys.sp_MScdc_capture_job', 
		@server=N'SQLPRD01-OSA', 
		@database_name=N'DBSMART_CTA', 
		@flags=4
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'CDC capture agent schedule.', 
		@enabled=1, 
		@freq_type=64, 
		@freq_interval=0, 
		@freq_subday_type=0, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20220716, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, 
		@schedule_uid=N'508f4d66-f6bf-4404-83cf-15ebe1255373'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Recorrente', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=8, 
		@freq_subday_interval=1, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20211218, 
		@active_end_date=99991231, 
		@active_start_time=3000, 
		@active_end_time=235959, 
		@schedule_uid=N'fb64b45a-728d-4431-afb7-14f75a3a09ed'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [cdc.DBSMART_CTA_cleanup]    Script Date: 10/6/2023 9:29:04 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [REPL-Checkup]    Script Date: 10/6/2023 9:29:04 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'REPL-Checkup' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'REPL-Checkup'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'cdc.DBSMART_CTA_cleanup', 
		@enabled=1, 
		@notify_level_eventlog=2, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'CDC Cleanup Job', 
		@category_name=N'REPL-Checkup', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Change Data Capture Cleanup Agent]    Script Date: 10/6/2023 9:29:04 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Change Data Capture Cleanup Agent', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=10, 
		@retry_interval=1, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'sys.sp_MScdc_cleanup_job', 
		@server=N'SQLPRD01-OSA', 
		@database_name=N'DBSMART_CTA', 
		@flags=4
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'CDC cleanup agent schedule.', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=1, 
		@freq_relative_interval=1, 
		@freq_recurrence_factor=0, 
		@active_start_date=20220716, 
		@active_end_date=99991231, 
		@active_start_time=20000, 
		@active_end_time=235959, 
		@schedule_uid=N'2560c14e-2034-433e-a400-9e9ea8bca56a'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [Coleta Fragmentacao Index DBSMART_CREDITO]    Script Date: 10/6/2023 9:29:04 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/6/2023 9:29:04 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Coleta Fragmentacao Index DBSMART_CREDITO', 
		@enabled=0, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Coleta]    Script Date: 10/6/2023 9:29:04 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Coleta', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'USE DBSMART_CREDITO
GO

INSERT INTO ADMIN_DBA.dbo.Index_fragmentation
SELECT GETDATE() collection_time,
DB_NAME(indexstats.database_id) as DBNAME,
dbschemas.[name] as ''Schema'',
dbtables.[name] as ''Table'',
dbindexes.[name] as ''Index'',
indexstats.avg_fragmentation_in_percent,
indexstats.page_count
FROM sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL, NULL, NULL) AS indexstats
INNER JOIN sys.tables dbtables on dbtables.[object_id] = indexstats.[object_id]
INNER JOIN sys.schemas dbschemas on dbtables.[schema_id] = dbschemas.[schema_id]
INNER JOIN sys.indexes AS dbindexes ON dbindexes.[object_id] = indexstats.[object_id]
AND indexstats.index_id = dbindexes.index_id
WHERE indexstats.database_id = DB_ID()
and indexstats.avg_fragmentation_in_percent > 15
ORDER BY indexstats.avg_fragmentation_in_percent desc;', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Coleta Fragmentacao Index', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=8, 
		@freq_subday_interval=1, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20190313, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, 
		@schedule_uid=N'5b1ecd19-578f-47e2-9eb4-d906aef10104'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [Coleta-sp_whoisactive]    Script Date: 10/6/2023 9:29:04 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Data Collector]    Script Date: 10/6/2023 9:29:04 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Data Collector' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Data Collector'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Coleta-sp_whoisactive', 
		@enabled=0, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'Data Collector', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [DBA - spWhoisActive]    Script Date: 10/6/2023 9:29:04 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'DBA - spWhoisActive', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'/* Creating temp table */

--IF NOT EXISTS (SELECT * FROM tempdb.sys.objects WHERE name=''monitoring_output'' and type=''U'')
--Remove temp table -- New syntax in 2016
DROP TABLE IF EXISTS tempdb.dbo.monitoring_output
BEGIN
 DECLARE @s VARCHAR(MAX)
 EXEC ADMIN_DBA.[dbo].sp_WhoIsActive 
  @output_column_list = ''[collection_time],[d%],[database_name],[session_id],[blocking_session_id],[blocked_session_count],
    [sql_text],[login_name],[wait_info],[query_plan],[additional_info],[status],[percent_complete],[host_name],
    [database_name],[sql_command],[CPU],[used_memory],[reads],[writes],[program_name],[start_time]'', 
  @return_schema = 1,
  @get_outer_command=1,
  @get_plans=1, 
  @get_additional_info=1, 
  @find_block_leaders=1,
  @schema = @s OUTPUT

 SET @s = REPLACE(@s, ''<table_name>'', ''tempdb.dbo.monitoring_output'')
 EXEC(@s)
END

/* Collecting sp_whoisactive result */ 

USE ADMIN_DBA
GO

declare @origin_db varchar(30) = ''DBSMART_MASTER''
declare @destination_db varchar(30) = ''ADMIN_DBA''

IF [master].sys.fn_hadr_is_primary_replica(@origin_db) = 1 and [master].sys.fn_hadr_is_primary_replica(@destination_db) = 1
BEGIN
 EXEC ADMIN_DBA.[dbo].sp_WhoIsActive @get_outer_command=1, @get_plans=1, @get_additional_info=1, @find_block_leaders=1,
    @output_column_list = ''[collection_time],[d%],[database_name],[session_id],[blocking_session_id],[blocked_session_count],
    [sql_text],[login_name],[wait_info],[query_plan],[additional_info],[status],[percent_complete],[host_name],
    [sql_command],[CPU],[used_memory],[reads],[writes],[program_name],[start_time]'',
  @destination_table = ''tempdb.dbo.monitoring_output''

 /* Merging collect_sequence with procedure result into persistent table */ 

 --Get next sequence value
 DECLARE @CollectId AS INT = NEXT VALUE FOR dbo.CollectId

 INSERT INTO ADMIN_DBA.[dbo].Resultado_WhoisActive
 SELECT [collection_time],@CollectId as ''Collection_Id'',[dd hh:mm:ss.mss],[database_name],[session_id],[blocking_session_id],[blocked_session_count],
    [sql_text],[login_name],[wait_info],[query_plan],[additional_info],[status],[percent_complete],[host_name],
    [sql_command],[CPU],[used_memory],[reads],[writes],[program_name],[start_time] from tempdb.dbo.monitoring_output

 --Purge Result table
 --Delete ADMIN_DBA.[dbo].Resultado_WhoisActive where collection_time <= getdate()-1
END', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'DBA - sp_WhoisActive', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=4, 
		@freq_subday_interval=1, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20190313, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, 
		@schedule_uid=N'ff8f4416-b34a-4ec0-b764-530b83780cd7'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [CommandLog Cleanup]    Script Date: 10/6/2023 9:29:04 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Database Maintenance]    Script Date: 10/6/2023 9:29:04 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Database Maintenance' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Database Maintenance'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'CommandLog Cleanup', 
		@enabled=0, 
		@notify_level_eventlog=2, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Source: https://ola.hallengren.com', 
		@category_name=N'Database Maintenance', 
		@owner_login_name=N'sa', 
		@notify_email_operator_name=N'Alerta', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [CommandLog Cleanup]    Script Date: 10/6/2023 9:29:04 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'CommandLog Cleanup', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DELETE FROM [dbo].[CommandLog]
WHERE StartTime < DATEADD(dd,-30,GETDATE())', 
		@database_name=N'ADMIN_DBA', 
		@output_file_name=N'$(ESCAPE_SQUOTE(SQLLOGDIR))\$(ESCAPE_SQUOTE(JOBNAME))_$(ESCAPE_SQUOTE(STEPID))_$(ESCAPE_SQUOTE(STRTDT))_$(ESCAPE_SQUOTE(STRTTM)).txt', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [Database Maintenance - CHECKDB - Weekly]    Script Date: 10/6/2023 9:29:04 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Database Maintenance]    Script Date: 10/6/2023 9:29:04 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Database Maintenance' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Database Maintenance'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Database Maintenance - CHECKDB - Weekly', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Job de Manuten��o Semanal para checagem de integridade

Source: https://ola.hallengren.com', 
		@category_name=N'Database Maintenance', 
		@owner_login_name=N'sa', 
		@notify_email_operator_name=N'Alerta', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [DBCC CHECKDB]    Script Date: 10/6/2023 9:29:04 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'DBCC CHECKDB', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'USE ADMIN_DBA
GO

declare @origin_db varchar(30) = ''DBSMART''
declare @destination_db varchar(30) = ''ADMIN_DBA''

IF [master].sys.fn_hadr_is_primary_replica(@origin_db) = 1 and [master].sys.fn_hadr_is_primary_replica(@destination_db) = 1
 BEGIN
  EXECUTE ADMIN_DBA..DatabaseIntegrityCheck
  @Databases = ''ALL_DATABASES'',
  @DatabaseOrder= ''DATABASE_NAME_ASC'',
  @CheckCommands = ''CHECKDB'',
  @TimeLimit=18000, --5 Five Hours
  @LockTimeout=210, --Lock timeout of 3,5 min (in seconds)
  @LogToTable=Y,
  @Execute=''Y''
 END
ELSE
 BEGIN
  EXECUTE ADMIN_DBA..DatabaseIntegrityCheck
  @Databases = ''ALL_DATABASES'',
  @DatabaseOrder= ''DATABASE_NAME_ASC'',
  @CheckCommands = ''CHECKDB'',
  @TimeLimit=18000, --5 Five Hours
  @LockTimeout=210, --Lock timeout of 3,5 min (in seconds)
  @Execute=''Y''
 END
', 
		@database_name=N'master', 
		@output_file_name=N'E:\MSSQL\SQLAgent_Logging\DBCC_CHECKDB_$(ESCAPE_SQUOTE(DATE))_$(ESCAPE_SQUOTE(TIME)).log', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'DBCC_CHECKDB', 
		@enabled=1, 
		@freq_type=8, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20190423, 
		@active_end_date=99991231, 
		@active_start_time=80000, 
		@active_end_time=235959, 
		@schedule_uid=N'91940788-70e0-45b2-b828-1b7b3071abf4'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [Database Maintenance (AGs) - Reindex & Statistics - Daily]    Script Date: 10/6/2023 9:29:04 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Database Maintenance]    Script Date: 10/6/2023 9:29:04 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Database Maintenance' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Database Maintenance'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Database Maintenance (AGs) - Reindex & Statistics - Daily', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Job de Manuten��o para Reindex e Update Statistics

Source: https://ola.hallengren.com', 
		@category_name=N'Database Maintenance', 
		@owner_login_name=N'sa', 
		@notify_email_operator_name=N'Alerta', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Rebuild Index and Update Statistics]    Script Date: 10/6/2023 9:29:04 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Rebuild Index and Update Statistics', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'USE ADMIN_DBA
GO

--SMART first when primary
EXECUTE ADMIN_DBA..IndexOptimize
 @Databases = ''DBSMART, DBSMART_CREDITO, DBSMART_CTA, DBSMART_TESOURARIA'',
 @DatabaseOrder=''DATABASE_NAME_ASC'',
 @FragmentationLow = NULL,
 @FragmentationMedium = ''INDEX_REORGANIZE,INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE'',
 @FragmentationHigh = ''INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE'',
 @FragmentationLevel1 = 10,
 @FragmentationLevel2 = 30,
 @UpdateStatistics = ''ALL'',
 @OnlyModifiedStatistics = ''Y'',
 @Indexes = ''ALL_INDEXES'' , --All Indexes, except indexes on those table
 @FillFactor = 90,
 @TimeLimit=9000, --2,5 hours (in seconds)
 @LockTimeout=135, --Lock timeout in seconds
 @Execute=''Y''
 GO

--CRK first when primary
EXECUTE ADMIN_DBA..IndexOptimize
 @Databases = ''INFOCRK_B2B_B2C_GESTAO'',
 @DatabaseOrder=''DATABASE_NAME_ASC'',
 @FragmentationLow = NULL,
 @FragmentationMedium = ''INDEX_REORGANIZE,INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE'',
 @FragmentationHigh = ''INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE'',
 @FragmentationLevel1 = 10,
 @FragmentationLevel2 = 30,
 @UpdateStatistics = ''ALL'',
 @OnlyModifiedStatistics = ''Y'',
 @Indexes = ''ALL_INDEXES, -INFOCRK_B2B_B2C_GESTAO.dbo.ProdutoPosicaoValor, -INFOCRK_B2B_B2C_GESTAO.dbo.ProdutoPosicao, -INFOCRK_B2B_B2C_GESTAO.dbo.PosicaoConsolidadaHistorica'' ,  --All Indexes, except indexes on those table
 @FillFactor = 90,
 @TimeLimit=9000, --2,5 hours (in seconds)
 @LockTimeout=135, --Lock timeout in seconds
 @Execute=''Y''
 GO

declare @destination_db varchar(30) = ''ADMIN_DBA''

IF [master].sys.fn_hadr_is_primary_replica(@destination_db) = 1
 BEGIN
  EXECUTE ADMIN_DBA..IndexOptimize
  @Databases = ''AVAILABILITY_GROUP_DATABASES, -UiPath, -INFOCRK_B2B_B2C_GESTAO, -INFOCRK_B2B_B2C_INTERMEDIARIA, -DBSMART, -DBSMART_CREDITO, -DBSMART_CTA, -DBSMART_TESOURARIA'',
  @DatabaseOrder=''DATABASE_NAME_ASC'',
  @FragmentationLow = NULL,
  @FragmentationMedium = ''INDEX_REORGANIZE,INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE'',
  @FragmentationHigh = ''INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE'',
  @FragmentationLevel1 = 10,
  @FragmentationLevel2 = 30,
  @UpdateStatistics = ''ALL'',
  @OnlyModifiedStatistics = ''Y'',
  @Indexes = ''ALL_INDEXES, -ModalOMS.dbo.ServiceLogs, -[ModalOMS].[dbo].[ServiceQueueResults]'', --All Indexes, except indexes on those table
  @FillFactor = 90,
  @TimeLimit=9000, --2,5 hours (in seconds)
  @LockTimeout=135, --Lock timeout in seconds
  @LogToTable=Y,
  @Execute=''Y''
 END
ELSE
 BEGIN
  EXECUTE ADMIN_DBA..IndexOptimize
  @Databases = ''AVAILABILITY_GROUP_DATABASES, -UiPath, -INFOCRK_B2B_B2C_GESTAO, -INFOCRK_B2B_B2C_INTERMEDIARIA, -DBSMART, -DBSMART_CREDITO, -DBSMART_CTA, -DBSMART_TESOURARIA'',
  @DatabaseOrder=''DATABASE_NAME_ASC'',
  @FragmentationLow = NULL,
  @FragmentationMedium = ''INDEX_REORGANIZE,INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE'',
  @FragmentationHigh = ''INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE'',
  @FragmentationLevel1 = 10,
  @FragmentationLevel2 = 30,
  @UpdateStatistics = ''ALL'',
  @OnlyModifiedStatistics = ''Y'',
  @Indexes = ''ALL_INDEXES, -ModalOMS.dbo.ServiceLogs, -[ModalOMS].[dbo].[ServiceQueueResults]'', --All Indexes, except indexes on those table
  @FillFactor = 90,
  @TimeLimit=9000, --2,5 hours (in seconds)
  @LockTimeout=135, --Lock timeout in seconds
  @Execute=''Y''
 END
', 
		@database_name=N'master', 
		@output_file_name=N'E:\MSSQL\SQLAgent_Logging\Daily-Maintenance_AGs_$(ESCAPE_SQUOTE(DATE))_$(ESCAPE_SQUOTE(TIME)).log', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Daily - Rebuild & Update Statistics (AGs)', 
		@enabled=1, 
		@freq_type=8, 
		@freq_interval=63, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20210513, 
		@active_end_date=99991231, 
		@active_start_time=20500, 
		@active_end_time=235959, 
		@schedule_uid=N'6b47edf0-22cd-4c4b-9a35-c0bb314e85fb'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [Database Maintenance (AGs) - Reindex & Statistics - Weekly]    Script Date: 10/6/2023 9:29:04 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Database Maintenance]    Script Date: 10/6/2023 9:29:04 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Database Maintenance' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Database Maintenance'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Database Maintenance (AGs) - Reindex & Statistics - Weekly', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Manuten��o Semanal para Reindex e Update Statistics

Source: https://ola.hallengren.com', 
		@category_name=N'Database Maintenance', 
		@owner_login_name=N'sa', 
		@notify_email_operator_name=N'Alerta', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Rebuild Index and Update Statistics]    Script Date: 10/6/2023 9:29:04 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Rebuild Index and Update Statistics', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'USE ADMIN_DBA
GO

declare @destination_db varchar(30) = ''ADMIN_DBA''

IF [master].sys.fn_hadr_is_primary_replica(@destination_db) = 1
 BEGIN
  EXECUTE ADMIN_DBA..IndexOptimize
  @Databases = ''AVAILABILITY_GROUP_DATABASES'',
  @DatabaseOrder=''DATABASE_NAME_ASC'',
  @FragmentationLow = NULL,
  @FragmentationMedium = ''INDEX_REORGANIZE,INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE'',
  @FragmentationHigh = ''INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE'',
  @FragmentationLevel1 = 5,
  @FragmentationLevel2 = 20,
  @UpdateStatistics = ''ALL'',
  @OnlyModifiedStatistics = ''Y'',
  @FillFactor = 90,
  @TimeLimit=72000, --20 hours (in seconds)
  @LockTimeout=210, --Lock timeout of 3,5 min (in seconds)
  @LogToTable=Y,
  @Execute=''Y''
 END
ELSE
 BEGIN
  EXECUTE ADMIN_DBA..IndexOptimize
  @Databases = ''AVAILABILITY_GROUP_DATABASES'',
  @DatabaseOrder=''DATABASE_NAME_ASC'',
  @FragmentationLow = NULL,
  @FragmentationMedium = ''INDEX_REORGANIZE,INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE'',
  @FragmentationHigh = ''INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE'',
  @FragmentationLevel1 = 5,
  @FragmentationLevel2 = 20,
  @UpdateStatistics = ''ALL'',
  @OnlyModifiedStatistics = ''Y'',
  @FillFactor = 90,
  @TimeLimit=72000, --20 hours (in seconds)
  @LockTimeout=210, --Lock timeout of 3,5 min (in seconds)
  @Execute=''Y''
 END
', 
		@database_name=N'master', 
		@output_file_name=N'E:\MSSQL\SQLAgent_Logging\Weekly-Maintenance_AGs_$(ESCAPE_SQUOTE(DATE))_$(ESCAPE_SQUOTE(TIME)).log', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Weekly - Rebuild & Update Statistics (AGs)', 
		@enabled=1, 
		@freq_type=8, 
		@freq_interval=64, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20211107, 
		@active_end_date=99991231, 
		@active_start_time=81500, 
		@active_end_time=235959, 
		@schedule_uid=N'd9d9a5e8-63b0-4318-85df-84596597eda5'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [Database Maintenance (Local DBs) - Reindex & Statistics - Daily]    Script Date: 10/6/2023 9:29:04 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Database Maintenance]    Script Date: 10/6/2023 9:29:04 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Database Maintenance' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Database Maintenance'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Database Maintenance (Local DBs) - Reindex & Statistics - Daily', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Job de Manuten��o para Reindex e Update Statistics

Source: https://ola.hallengren.com', 
		@category_name=N'Database Maintenance', 
		@owner_login_name=N'sa', 
		@notify_email_operator_name=N'Alerta', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Rebuild Index and Update Statistics]    Script Date: 10/6/2023 9:29:04 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Rebuild Index and Update Statistics', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'USE ADMIN_DBA
GO

declare @destination_db varchar(30) = ''ADMIN_DBA''

-- /*IF [master].sys.fn_hadr_is_primary_replica(@origin_db) = 1 and [master].sys.fn_hadr_is_primary_replica(@destination_db) = 1*/
IF [master].sys.fn_hadr_is_primary_replica(@destination_db) = 1
 BEGIN
  EXECUTE ADMIN_DBA..IndexOptimize
  @Databases = ''USER_DATABASES, -AVAILABILITY_GROUP_DATABASES, -EGUARDIAN_HIST'',
  @DatabaseOrder=''DATABASE_NAME_ASC'',
  @FragmentationLow = NULL,
  @FragmentationMedium = ''INDEX_REORGANIZE,INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE'',
  @FragmentationHigh = ''INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE'',
  @FragmentationLevel1 = 5,
  @FragmentationLevel2 = 30,
  @UpdateStatistics = ''ALL'',
  @OnlyModifiedStatistics = ''Y'',
  @Indexes = ''ALL_INDEXES, -EGUARDIAN.dbo.TB_INTEG_ORDENS_SINACOR, -[ModalOMS].[dbo].[ServiceQueueResults]'', --All Indexes, except indexes on those table
  @FillFactor = 90,
  @TimeLimit=9000, --2,5 hours (in seconds)
  @LockTimeout=135, --Lock timeout of in seconds
  @LogToTable=Y,
  @Execute=''Y''
 END
ELSE
 BEGIN
  EXECUTE ADMIN_DBA..IndexOptimize
  @Databases = ''USER_DATABASES, -AVAILABILITY_GROUP_DATABASES, -EGUARDIAN_HIST'',
  @DatabaseOrder=''DATABASE_NAME_ASC'',
  @FragmentationLow = NULL,
  @FragmentationMedium = ''INDEX_REORGANIZE,INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE'',
  @FragmentationHigh = ''INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE'',
  @FragmentationLevel1 = 5,
  @FragmentationLevel2 = 30,
  @UpdateStatistics = ''ALL'',
  @OnlyModifiedStatistics = ''Y'',
  @Indexes = ''ALL_INDEXES, -EGUARDIAN.dbo.TB_INTEG_ORDENS_SINACOR, -[ModalOMS].[dbo].[ServiceQueueResults]'', --All Indexes, except indexes on those table
  @FillFactor = 90,
  @TimeLimit=9000, --2,5 hours (in seconds)
  @LockTimeout=135, --Lock timeout of in seconds
  @Execute=''Y''
 END
', 
		@database_name=N'master', 
		@output_file_name=N'E:\MSSQL\SQLAgent_Logging\Daily-Maintenance_LocalDBs_$(ESCAPE_SQUOTE(DATE))_$(ESCAPE_SQUOTE(TIME)).log', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Daily - Rebuild & Update Statistics (Local DBs)', 
		@enabled=1, 
		@freq_type=8, 
		@freq_interval=63, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20201027, 
		@active_end_date=99991231, 
		@active_start_time=192500, 
		@active_end_time=235959, 
		@schedule_uid=N'31c4f3b1-dc3e-4506-a0cc-d469928febc3'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [Database Maintenance (Local DBs) - Reindex & Statistics - Weekly]    Script Date: 10/6/2023 9:29:04 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Database Maintenance]    Script Date: 10/6/2023 9:29:04 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Database Maintenance' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Database Maintenance'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Database Maintenance (Local DBs) - Reindex & Statistics - Weekly', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Manuten��o Semanal para Reindex e Update Statistics

Source: https://ola.hallengren.com', 
		@category_name=N'Database Maintenance', 
		@owner_login_name=N'sa', 
		@notify_email_operator_name=N'Alerta', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Rebuild Index and Update Statistics]    Script Date: 10/6/2023 9:29:04 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Rebuild Index and Update Statistics', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'USE ADMIN_DBA
GO

declare @destination_db varchar(30) = ''ADMIN_DBA''

IF [master].sys.fn_hadr_is_primary_replica(@destination_db) = 1
 BEGIN
  EXECUTE ADMIN_DBA..IndexOptimize
  @Databases = ''USER_DATABASES, -AVAILABILITY_GROUP_DATABASES'',
  @DatabaseOrder=''DATABASE_NAME_ASC'',
  @FragmentationLow = NULL,
  @FragmentationMedium = ''INDEX_REORGANIZE,INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE'',
  @FragmentationHigh = ''INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE'',
  @FragmentationLevel1 = 2,
  @FragmentationLevel2 = 20,
  @UpdateStatistics = ''ALL'',
  @OnlyModifiedStatistics = ''Y'',
  @FillFactor = 90,
  @TimeLimit=28800, --8 hours (in seconds)
  @LockTimeout=210, --Lock timeout of 3,5 min (in seconds)
  @LogToTable=Y,
  @Execute=''Y''
 END
ELSE
 BEGIN
  EXECUTE ADMIN_DBA..IndexOptimize
  @Databases = ''USER_DATABASES, -AVAILABILITY_GROUP_DATABASES'',
  @DatabaseOrder=''DATABASE_NAME_ASC'',
  @FragmentationLow = NULL,
  @FragmentationMedium = ''INDEX_REORGANIZE,INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE'',
  @FragmentationHigh = ''INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE'',
  @FragmentationLevel1 = 2,
  @FragmentationLevel2 = 20,
  @UpdateStatistics = ''ALL'',
  @OnlyModifiedStatistics = ''Y'',
  @FillFactor = 90,
  @TimeLimit=28800, --8 hours (in seconds)
  @LockTimeout=210, --Lock timeout of 3,5 min (in seconds)
  @Execute=''Y''
 END
', 
		@database_name=N'master', 
		@output_file_name=N'E:\MSSQL\SQLAgent_Logging\Weekly-Maintenance_LocalDBs_$(ESCAPE_SQUOTE(DATE))_$(ESCAPE_SQUOTE(TIME)).log', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Weekly - Rebuild & Update Statistics (Local DBs)', 
		@enabled=1, 
		@freq_type=8, 
		@freq_interval=64, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20201028, 
		@active_end_date=99991231, 
		@active_start_time=10500, 
		@active_end_time=235959, 
		@schedule_uid=N'436e3651-60bf-4288-b324-4faaf5f99868'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [DatabaseBackup - SYSTEM_DATABASES - FULL]    Script Date: 10/6/2023 9:29:04 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Database Maintenance]    Script Date: 10/6/2023 9:29:04 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Database Maintenance' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Database Maintenance'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'DatabaseBackup - SYSTEM_DATABASES - FULL', 
		@enabled=0, 
		@notify_level_eventlog=2, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Source: https://ola.hallengren.com', 
		@category_name=N'Database Maintenance', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [DatabaseBackup - SYSTEM_DATABASES - FULL]    Script Date: 10/6/2023 9:29:04 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'DatabaseBackup - SYSTEM_DATABASES - FULL', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXECUTE [dbo].[DatabaseBackup]
@Databases = ''SYSTEM_DATABASES'',
@Directory = NULL,
@BackupType = ''FULL'',
@Verify = ''Y'',
@CleanupTime = NULL,
@CheckSum = ''Y'',
@LogToTable = ''Y''', 
		@database_name=N'ADMIN_DBA', 
		@output_file_name=N'$(ESCAPE_SQUOTE(SQLLOGDIR))\$(ESCAPE_SQUOTE(JOBNAME))_$(ESCAPE_SQUOTE(STEPID))_$(ESCAPE_SQUOTE(DATE))_$(ESCAPE_SQUOTE(TIME)).txt', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [DatabaseBackup - USER_DATABASES - DIFF]    Script Date: 10/6/2023 9:29:04 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Database Maintenance]    Script Date: 10/6/2023 9:29:04 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Database Maintenance' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Database Maintenance'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'DatabaseBackup - USER_DATABASES - DIFF', 
		@enabled=0, 
		@notify_level_eventlog=2, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Source: https://ola.hallengren.com', 
		@category_name=N'Database Maintenance', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [DatabaseBackup - USER_DATABASES - DIFF]    Script Date: 10/6/2023 9:29:05 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'DatabaseBackup - USER_DATABASES - DIFF', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXECUTE [dbo].[DatabaseBackup]
@Databases = ''USER_DATABASES'',
@Directory = NULL,
@BackupType = ''DIFF'',
@Verify = ''Y'',
@CleanupTime = NULL,
@CheckSum = ''Y'',
@LogToTable = ''Y''', 
		@database_name=N'ADMIN_DBA', 
		@output_file_name=N'$(ESCAPE_SQUOTE(SQLLOGDIR))\$(ESCAPE_SQUOTE(JOBNAME))_$(ESCAPE_SQUOTE(STEPID))_$(ESCAPE_SQUOTE(DATE))_$(ESCAPE_SQUOTE(TIME)).txt', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [DatabaseBackup - USER_DATABASES - FULL]    Script Date: 10/6/2023 9:29:05 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Database Maintenance]    Script Date: 10/6/2023 9:29:05 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Database Maintenance' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Database Maintenance'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'DatabaseBackup - USER_DATABASES - FULL', 
		@enabled=0, 
		@notify_level_eventlog=2, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Source: https://ola.hallengren.com', 
		@category_name=N'Database Maintenance', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [DatabaseBackup - USER_DATABASES - FULL]    Script Date: 10/6/2023 9:29:05 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'DatabaseBackup - USER_DATABASES - FULL', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXECUTE [dbo].[DatabaseBackup]
@Databases = ''USER_DATABASES'',
@Directory = NULL,
@BackupType = ''FULL'',
@Verify = ''Y'',
@CleanupTime = NULL,
@CheckSum = ''Y'',
@LogToTable = ''Y''', 
		@database_name=N'ADMIN_DBA', 
		@output_file_name=N'$(ESCAPE_SQUOTE(SQLLOGDIR))\$(ESCAPE_SQUOTE(JOBNAME))_$(ESCAPE_SQUOTE(STEPID))_$(ESCAPE_SQUOTE(DATE))_$(ESCAPE_SQUOTE(TIME)).txt', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [DatabaseBackup - USER_DATABASES - LOG]    Script Date: 10/6/2023 9:29:05 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Database Maintenance]    Script Date: 10/6/2023 9:29:05 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Database Maintenance' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Database Maintenance'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'DatabaseBackup - USER_DATABASES - LOG', 
		@enabled=0, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Source: https://ola.hallengren.com', 
		@category_name=N'Database Maintenance', 
		@owner_login_name=N'sa', 
		@notify_email_operator_name=N'Alerta', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [DatabaseBackup - USER_DATABASES - LOG]    Script Date: 10/6/2023 9:29:05 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'DatabaseBackup - USER_DATABASES - LOG', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'

EXECUTE ADMIN_DBA.dbo.DatabaseBackup
@Databases = ''USER_DATABASES'',
--@AvailabilityGroups = ''AG_11'',
@Directory = ''nul'',
@BackupType = ''LOG'', -- FULL,DIFF,LOG
--@CopyOnly = ''Y'',
@Compress = ''Y'',
@MaxFileSize = 10240,  --10GB
--@CleanupTime = 1, -- 1 days (in hours)
@MirrorCleanupMode = ''AFTER_BACKUP'', 

@DirectoryStructure = ''{ServerName}{DirectorySeparator}{DatabaseName}{DirectorySeparator}{BackupType}_{Partial}_{CopyOnly}'',

@FileName = ''{DatabaseName}-{BackupType}-{CopyOnly}-{Year}{Month}{Day}_{Hour}{Minute}{Second}-{FileNumber}.{FileExtension}'',

@AvailabilityGroupDirectoryStructure = ''{ClusterName}_{AvailabilityGroupName}{DirectorySeparator}{DatabaseName}{DirectorySeparator}{BackupType}_{Partial}_{CopyOnly}'',

@AvailabilityGroupFileName = ''{DatabaseName}-{BackupType}-{CopyOnly}-{Year}{Month}{Day}_{Hour}{Minute}{Second}-{FileNumber}.{FileExtension}''', 
		@database_name=N'ADMIN_DBA', 
		@flags=4
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'30min', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=4, 
		@freq_subday_interval=15, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20210305, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, 
		@schedule_uid=N'7175704d-2c27-4757-ac98-fda96b6ccb62'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [DatabaseIntegrityCheck - SYSTEM_DATABASES]    Script Date: 10/6/2023 9:29:05 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Database Maintenance]    Script Date: 10/6/2023 9:29:05 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Database Maintenance' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Database Maintenance'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'DatabaseIntegrityCheck - SYSTEM_DATABASES', 
		@enabled=0, 
		@notify_level_eventlog=2, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Source: https://ola.hallengren.com', 
		@category_name=N'Database Maintenance', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [DatabaseIntegrityCheck - SYSTEM_DATABASES]    Script Date: 10/6/2023 9:29:05 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'DatabaseIntegrityCheck - SYSTEM_DATABASES', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXECUTE [dbo].[DatabaseIntegrityCheck]
@Databases = ''SYSTEM_DATABASES'',
@LogToTable = ''Y''', 
		@database_name=N'ADMIN_DBA', 
		@output_file_name=N'$(ESCAPE_SQUOTE(SQLLOGDIR))\$(ESCAPE_SQUOTE(JOBNAME))_$(ESCAPE_SQUOTE(STEPID))_$(ESCAPE_SQUOTE(DATE))_$(ESCAPE_SQUOTE(TIME)).txt', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [DatabaseIntegrityCheck - USER_DATABASES]    Script Date: 10/6/2023 9:29:05 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Database Maintenance]    Script Date: 10/6/2023 9:29:05 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Database Maintenance' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Database Maintenance'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'DatabaseIntegrityCheck - USER_DATABASES', 
		@enabled=0, 
		@notify_level_eventlog=2, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Source: https://ola.hallengren.com', 
		@category_name=N'Database Maintenance', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [DatabaseIntegrityCheck - USER_DATABASES]    Script Date: 10/6/2023 9:29:05 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'DatabaseIntegrityCheck - USER_DATABASES', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXECUTE [dbo].[DatabaseIntegrityCheck]
@Databases = ''USER_DATABASES'',
@LogToTable = ''Y''', 
		@database_name=N'ADMIN_DBA', 
		@output_file_name=N'$(ESCAPE_SQUOTE(SQLLOGDIR))\$(ESCAPE_SQUOTE(JOBNAME))_$(ESCAPE_SQUOTE(STEPID))_$(ESCAPE_SQUOTE(DATE))_$(ESCAPE_SQUOTE(TIME)).txt', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [DBA - Auditoria]    Script Date: 10/6/2023 9:29:05 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/6/2023 9:29:05 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'DBA - Auditoria', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', 
		@notify_email_operator_name=N'Alerta', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Step_01]    Script Date: 10/6/2023 9:29:05 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Step_01', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=4, 
		@on_success_step_id=2, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'USE ADMIN_DBA
GO

exec [ADMIN_DBA].[dbo].[Audit_Permission_Job]', 
		@database_name=N'master', 
		@flags=4
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Step_02]    Script Date: 10/6/2023 9:29:05 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Step_02', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'USE ADMIN_DBA
GO

exec [ADMIN_DBA].[dbo].[Audit_Login_Job]

', 
		@database_name=N'master', 
		@flags=4
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Sched_01', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20230104, 
		@active_end_date=99991231, 
		@active_start_time=62000, 
		@active_end_time=235959, 
		@schedule_uid=N'984afbdb-b0cd-47e7-8c1c-e8fb1f495c89'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [DBA - Auditoria Windows User]    Script Date: 10/6/2023 9:29:05 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/6/2023 9:29:05 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'DBA - Auditoria Windows User', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', 
		@notify_email_operator_name=N'Alerta', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Step_01]    Script Date: 10/6/2023 9:29:05 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Step_01', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'USE ADMIN_DBA
GO

exec [ADMIN_DBA].[dbo].[Audit_User_Database_Job]', 
		@database_name=N'master', 
		@flags=4
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Sched_01', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20230104, 
		@active_end_date=99991231, 
		@active_start_time=62000, 
		@active_end_time=235959, 
		@schedule_uid=N'984afbdb-b0cd-47e7-8c1c-e8fb1f495c89'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [DBA - Coleta File Auto Grow Logs]    Script Date: 10/6/2023 9:29:05 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/6/2023 9:29:05 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'DBA - Coleta File Auto Grow Logs', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', 
		@notify_email_operator_name=N'Alerta', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Step01]    Script Date: 10/6/2023 9:29:05 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Step01', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'exec admin_dba.dbo.File_Auto_Grow_Job', 
		@database_name=N'ADMIN_DBA', 
		@flags=4
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Schedule', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20220809, 
		@active_end_date=99991231, 
		@active_start_time=60000, 
		@active_end_time=235959, 
		@schedule_uid=N'86684850-3622-4759-8435-4744b8df8afc'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [DBA - Coleta Sessoes]    Script Date: 10/6/2023 9:29:05 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/6/2023 9:29:05 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'DBA - Coleta Sessoes', 
		@enabled=0, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', 
		@notify_email_operator_name=N'Alerta', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [step]    Script Date: 10/6/2023 9:29:05 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'step', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'USE [ADMIN_DBA]
GO

EXEC [dbo].[Coleta_Sessoes]', 
		@database_name=N'master', 
		@flags=4
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'sched01', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=4, 
		@freq_subday_interval=30, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20220202, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, 
		@schedule_uid=N'0e5be8cd-9aaa-446e-9d1d-5e7ad8a73ba8'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [DBA - Copia_Logins]    Script Date: 10/6/2023 9:29:05 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Database Maintenance]    Script Date: 10/6/2023 9:29:05 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Database Maintenance' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Database Maintenance'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'DBA - Copia_Logins', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Copia novos logins para os servidores secund�rios', 
		@category_name=N'Database Maintenance', 
		@owner_login_name=N'MODAL\SVC_SQL', 
		@notify_email_operator_name=N'Alerta', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [sql_copy_jobs]    Script Date: 10/6/2023 9:29:05 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'sql_copy_jobs', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'PowerShell', 
		@command=N'powershell -executionpolicy bypass -c "C:\PsScripts\copy-new-logins-to-secondaries.ps1"', 
		@database_name=N'master', 
		@output_file_name=N'E:\MSSQL\SQLAgent_Logging\copy-new-logins-to-secondaries_$(ESCAPE_SQUOTE(DATE)).log', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'DBA - Copia_Jobs', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=4, 
		@freq_subday_interval=10, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20191031, 
		@active_end_date=99991231, 
		@active_start_time=500, 
		@active_end_time=235959, 
		@schedule_uid=N'd3657ea5-6b7d-4889-8e29-166ed66cfeb8'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [DBA - Expurgo_Tabelas (Rebuild Heap)]    Script Date: 10/6/2023 9:29:05 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/6/2023 9:29:05 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'DBA - Expurgo_Tabelas (Rebuild Heap)', 
		@enabled=0, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Job para executar rebuild eventualmente para liberar o espa�o nos arquivos de dados das tabelas HEAP.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Step 01]    Script Date: 10/6/2023 9:29:05 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Step 01', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'USE DBSMART_CREDITO_LOG; ALTER TABLE TGlb_ProdutoAberturaFechamentoPendencias REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_CREDITO_LOG; ALTER TABLE TCrd_OperacaoCreditoLiberacao REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_CREDITO_LOG; ALTER TABLE TCrd_OperacaoCredito REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_CREDITO_LOG; ALTER TABLE TCrd_PlcArquivo REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_CREDITO_LOG; ALTER TABLE TCrd_Informacao3040Operacao REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_CREDITO_LOG; ALTER TABLE TCrd_OperacaoCreditoLiquidacao REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_CREDITO_LOG; ALTER TABLE TCrd_Informacao3040Cliente REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_CREDITO_LOG; ALTER TABLE TCrd_LimiteCreditoAnexo REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_CREDITO_LOG; ALTER TABLE TCrd_OperacaoCreditoContaGarantidaAmortizacao REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_CREDITO_LOG; ALTER TABLE TCrd_ContratoMae REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_CTA_LOG; ALTER TABLE TCta_SaldoBancario_ClassificaContaTitular REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_CTA_LOG; ALTER TABLE TCta_MovimentoBancario REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_CTA_LOG; ALTER TABLE TCta_SaldoBancario REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_CTA_LOG; ALTER TABLE TCta_IntegracaoBancoOnline REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_CTA_LOG; ALTER TABLE TCta_MovimentoBancarioReplicado REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_TESOURARIA_LOG; ALTER TABLE TTes_Integracao REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_TESOURARIA_LOG; ALTER TABLE TTes_RequisicaoMetadado REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_TESOURARIA_LOG; ALTER TABLE TTes_Requisicao REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_TESOURARIA_LOG; ALTER TABLE TTes_NPCServicoRequest REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_TESOURARIA_LOG; ALTER TABLE TTes_RequisicaoContabil REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_TESOURARIA_LOG; ALTER TABLE TTes_RequisicaoPessoa REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_TESOURARIA_LOG; ALTER TABLE TTes_MensagemRecebidaDadosSPB REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_TESOURARIA_LOG; ALTER TABLE TTes_ContaRequisicao REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_TESOURARIA_LOG; ALTER TABLE TTes_RequisicaoDadosSPB REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_TESOURARIA_LOG; ALTER TABLE TTes_RequisicaoPessoaConta REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_TESOURARIA_LOG; ALTER TABLE TTes_RequisicaoChaveIntegracao REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_TESOURARIA_LOG; ALTER TABLE TTes_RequisicaoGrupoContabil REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_TESOURARIA_LOG; ALTER TABLE TTes_RequisicaoStatusWorkflow REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_TESOURARIA_LOG; ALTER TABLE TTes_RequisicaoMovimentoBancario REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_TESOURARIA_LOG; ALTER TABLE TTes_RequisicaoLogIntegracao REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_TESOURARIA_LOG; ALTER TABLE TTes_RequisicaoDescricao REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_TESOURARIA_LOG; ALTER TABLE TTes_MensagemRecebida REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_TESOURARIA_LOG; ALTER TABLE TTes_MensagemRecebidaParticipantesDadosCorrigidos REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_TESOURARIA_LOG; ALTER TABLE TTes_MensagemRecebidaParticipantes REBUILD WITH (ONLINE = ON) ;
GO
USE DBSMART_TESOURARIA_LOG; ALTER TABLE TTes_MensagemRecebidaParticipantesOriginais REBUILD WITH (ONLINE = ON) ;
GO', 
		@database_name=N'master', 
		@flags=4
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Sched 01', 
		@enabled=0, 
		@freq_type=1, 
		@freq_interval=0, 
		@freq_subday_type=0, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20210918, 
		@active_end_date=99991231, 
		@active_start_time=210000, 
		@active_end_time=235959, 
		@schedule_uid=N'9d5c84e4-89c9-4fea-ab38-8e1c38debc3d'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Sched 02', 
		@enabled=0, 
		@freq_type=1, 
		@freq_interval=0, 
		@freq_subday_type=0, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20210919, 
		@active_end_date=99991231, 
		@active_start_time=210000, 
		@active_end_time=235959, 
		@schedule_uid=N'a3a323cd-59b6-4fdf-88e4-be08f3ba0227'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [DBA - Expurgo_Tabelas Diario]    Script Date: 10/6/2023 9:29:05 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/6/2023 9:29:05 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'DBA - Expurgo_Tabelas Diario', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Step_01]    Script Date: 10/6/2023 9:29:05 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Step_01', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'USE ADMIN_DBA
GO 


Delete_Job', 
		@database_name=N'master', 
		@output_file_name=N'E:\MSSQL\SQLAgent_Logging\Expurgo_Tabelas_$(ESCAPE_SQUOTE(DATE)).log', 
		@flags=6
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Daily', 
		@enabled=1, 
		@freq_type=8, 
		@freq_interval=62, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20210827, 
		@active_end_date=99991231, 
		@active_start_time=30000, 
		@active_end_time=235959, 
		@schedule_uid=N'7e917300-3094-48cf-85eb-fe25e711de74'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [DBA - Expurgo_Tabelas Semanal]    Script Date: 10/6/2023 9:29:06 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/6/2023 9:29:06 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'DBA - Expurgo_Tabelas Semanal', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Step_01]    Script Date: 10/6/2023 9:29:06 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Step_01', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'USE ADMIN_DBA
GO 


Delete_Job @limitSeconds = 43200  --12h', 
		@database_name=N'master', 
		@output_file_name=N'E:\MSSQL\SQLAgent_Logging\Expurgo_Tabelas_Semanal_$(ESCAPE_SQUOTE(DATE)).log', 
		@flags=6
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Weekly', 
		@enabled=1, 
		@freq_type=8, 
		@freq_interval=65, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20210525, 
		@active_end_date=99991231, 
		@active_start_time=80000, 
		@active_end_time=235959, 
		@schedule_uid=N'727dda70-c7cc-4591-a3a5-545640bd977f'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [DBA_ADVANCEDIT - Coleta dos tamanhos das bases de dados]    Script Date: 10/6/2023 9:29:06 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/6/2023 9:29:06 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'DBA_ADVANCEDIT - Coleta dos tamanhos das bases de dados', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Coleta o tamanho das bases de dados da inst�ncia.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [DBA_ADVANCEDIT - Executa coleta dos tamanhos de base]    Script Date: 10/6/2023 9:29:06 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'DBA_ADVANCEDIT - Executa coleta dos tamanhos de base', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'exec spColetaTamanhoDBs', 
		@database_name=N'DBA_ADVANCEDIT', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Todos os dias as 22h', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=5, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20121005, 
		@active_end_date=99991231, 
		@active_start_time=220000, 
		@active_end_time=235959, 
		@schedule_uid=N'c1ad0340-4fd2-44e7-916c-1c2c4a1db663'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [DBA_ADVANCEDIT - Coleta espa�o em disco]    Script Date: 10/6/2023 9:29:06 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/6/2023 9:29:06 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'DBA_ADVANCEDIT - Coleta espa�o em disco', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Coleta o espa�o em disco na inst�ncia.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [DBA_ADVANCEDIT - Exec dbo.usp_diskspace]    Script Date: 10/6/2023 9:29:06 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'DBA_ADVANCEDIT - Exec dbo.usp_diskspace', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'exec dbo.usp_diskspace', 
		@database_name=N'DBA_ADVANCEDIT', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Todos os dias 04 manha', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=5, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20121005, 
		@active_end_date=99991231, 
		@active_start_time=40000, 
		@active_end_time=235959, 
		@schedule_uid=N'66d69d34-b8d8-46e4-a474-f22cedd7cc79'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [DBA_ADVANCEDIT - Coleta tamanho tabelas]    Script Date: 10/6/2023 9:29:06 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/6/2023 9:29:06 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'DBA_ADVANCEDIT - Coleta tamanho tabelas', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Coleta_tamanho_tabela]    Script Date: 10/6/2023 9:29:06 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Coleta_tamanho_tabela', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'set nocount on
 
declare @vname   varchar(100)
declare @vbase   varchar(100)
declare @vschema nvarchar(100)
declare @sql     varchar(max) 
 
create table #tmpTamTabela (
  db         varchar(100) null
, name       varchar(100) null
, [rows]       bigint          null
, reserved   varchar(25)  null
, data       varchar(25)  null
, index_size varchar(25)  null
, unused     varchar(25)  null
 )
 
create table #tmpTabelas (
  tabela varchar(100) null, schema_usuario nvarchar(100)null
)
 
 declare ct cursor local fast_forward read_only for
select ag_db_name from sys.dm_hadr_cached_database_replica_states
where is_primary_replica =1
order by 1

 
open ct
 
while 0 =0 
begin
  fetch next from ct into @vbase
  if @@fetch_status <> 0 break 
   
  truncate table #tmpTabelas
  
  select @sql = ''
       
        insert into #tmpTabelas (tabela, schema_usuario)
          select sysobjects.name, usuario.table_schema
            from ['' + rtrim(@vbase) + '']..sysobjects
            inner join ['' + rtrim(@vbase) + ''].INFORMATION_SCHEMA.TABLES usuario on (usuario.TABLE_NAME=sysobjects.name)
           where type = ''''U''''
           
          ''
   
  exec(@sql)
 
  declare ct1 cursor local fast_forward read_only for
    select tabela, schema_usuario from #tmpTabelas
     order by 1
  
  open ct1
  
  while 1 = 1
  begin
    fetch next from ct1 into @vname, @vschema
    if @@fetch_status <> 0 break
     
    select @sql = ''
       
        insert into #tmpTamTabela (name, rows, reserved, data, index_size, unused)
          exec ['' + rtrim(@vbase) + '']..sp_spaceused '''''' + rtrim(@vschema) +''.['' + rtrim(@vname) + '']''''
           
        update #tmpTamTabela
           set db = '''''' + rtrim(@vbase) + ''''''
         where db is null
           
     ''
 
    exec(@sql)
     
  end
  close ct1
  deallocate ct1    
   
end
close ct
deallocate ct
use DBA_ADVANCEDIT
 insert into table_size (dbname, objname, nrrowCount, tamanhoMB,nrDadosMB, nrIndex, unUsedMB, datacoleta) 
 ( select db  
		,name
		,[rows] 
	   , convert(bigint, replace(reserved, '' KB'','''')) / 1024 -- Convers�o para MB
	   , convert(int, replace(data, '' KB'','''')) / 1024
     , convert(int, replace(index_size, '' KB'',''''))
     ,  convert(int, replace(unused, '' KB'',''''))/1024
    , getdate()
	from #tmpTamTabela
	where convert(bigint, replace(reserved, '' KB'','''')) / 1024 is not null
 )
 drop table  #tmpTamTabela 
 drop table #tmpTabelas
 
', 
		@database_name=N'DBA_ADVANCEDIT', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Schedule_coleta tamanho tabelas', 
		@enabled=1, 
		@freq_type=32, 
		@freq_interval=8, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=16, 
		@freq_recurrence_factor=1, 
		@active_start_date=20160812, 
		@active_end_date=99991231, 
		@active_start_time=230000, 
		@active_end_time=235959, 
		@schedule_uid=N'2c778215-9c78-4a56-a2be-1a12359039f7'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [DBA_ADVANCEDIT - Coleta Transacoes]    Script Date: 10/6/2023 9:29:06 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/6/2023 9:29:06 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'DBA_ADVANCEDIT - Coleta Transacoes', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'AdvancedIT - Job criado para coletar trasa��es do banco a cada 3 minutos.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Coleta transacoesSQL]    Script Date: 10/6/2023 9:29:06 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Coleta transacoesSQL', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'INSERT INTO [dbo].[SQLPERF]
SELECT req.session_id, 
db_name(req.database_id)Dbname, 
req.command, 
req.status, 
req.blocking_session_id as Blk_By, 
  SUBSTRING(sqltext.text, req.statement_start_offset / 2,
            1+( CASE WHEN req.statement_end_offset = -1
                   THEN LEN(CONVERT(NVARCHAR(MAX), sqltext.text)) * 2
                   ELSE req.statement_end_offset
              END - req.statement_start_offset ) / 2) AS sql_text , 
se.login_name, 
se.host_name,
se.program_name, 
req.cpu_time, 
CAST(((DATEDIFF(s,start_time,GetDate()))/3600) as varchar) + '' hour(s), '' + CAST((DATEDIFF(s,start_time,GetDate())%3600)/60 as varchar) + ''min, '' + CAST((DATEDIFF(s,start_time,GetDate())%60) as varchar) + '' sec'' as running_time, CAST((estimated_completion_time/3600000) as varchar) + '' hour(s), '' + CAST((estimated_completion_time %3600000)/60000 as varchar) + ''min, '' + CAST((estimated_completion_time %60000)/1000 as varchar) + '' sec'' as est_time_to_go, dateadd(second,estimated_completion_time/1000, getdate()) as est_completion_time, 
req.percent_complete, 
req.user_id, 
req.start_time, 
req.last_wait_type, 
req.reads, 
req.writes, 
req.logical_reads,
getdate()
FROM sys.dm_exec_requests req join sys.dm_exec_sessions se ON req.session_id = se.session_id 
CROSS APPLY sys.dm_exec_sql_text(sql_handle) AS sqltext 
where req.status IN (''suspended'', ''running'', ''runnable'')
and text not like ''sp_server_diagnostics''
and db_name(req.database_id) not like ''DBA_ADVANCEDIT''', 
		@database_name=N'DBA_ADVANCEDIT', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [ExpurgoTransa��es]    Script Date: 10/6/2023 9:29:06 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'ExpurgoTransa��es', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'delete from sqlperf where colletion_time <=GETDATE()-60', 
		@database_name=N'DBA_ADVANCEDIT', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'A cada 40 seg', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=2, 
		@freq_subday_interval=40, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20150615, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, 
		@schedule_uid=N'fe4bae9c-ac0a-4d8a-a8d4-bd0b0039680e'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [dba_modal_rotina - Checklist_Login_RPA]    Script Date: 10/6/2023 9:29:06 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/6/2023 9:29:06 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'dba_modal_rotina - Checklist_Login_RPA', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Rotina para validar e desativar os logins informados pelo RPA na rotina de checklist de desligamento.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', 
		@notify_email_operator_name=N'Alerta', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [step01]    Script Date: 10/6/2023 9:29:06 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'step01', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'USE ADMIN_DBA
GO

Checklist_Login_Job
', 
		@database_name=N'master', 
		@flags=4
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'sched', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20210928, 
		@active_end_date=99991231, 
		@active_start_time=220000, 
		@active_end_time=235959, 
		@schedule_uid=N'd06b95c0-4df7-4e4a-9952-090242ac6e64'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [dba_modal_rotina.info_audit_users]    Script Date: 10/6/2023 9:29:06 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/6/2023 9:29:06 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'dba_modal_rotina.info_audit_users', 
		@enabled=0, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Job para carga da tabela ADMIN_DBA..TB_INFO_AUDIT_USERS  que � utilizada para consulta dos usu�rios SQL no Report Server.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'MODAL\SVC_SQL', 
		@notify_email_operator_name=N'Alerta', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Step01]    Script Date: 10/6/2023 9:29:06 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Step01', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'exec ADMIN_DBA..[pr_info_audit_users] ', 
		@database_name=N'ADMIN_DBA', 
		@flags=4
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Diario_22h', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20200618, 
		@active_end_date=99991231, 
		@active_start_time=220000, 
		@active_end_time=235959, 
		@schedule_uid=N'660cad0c-32bd-4e19-8aff-7cee0adaf8bd'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [dba_modal_rotina.Volumetria]    Script Date: 10/6/2023 9:29:06 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/6/2023 9:29:06 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'dba_modal_rotina.Volumetria', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'MODAL\SVC_SQL', 
		@notify_email_operator_name=N'Alerta', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Step01]    Script Date: 10/6/2023 9:29:06 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Step01', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'exec ADMIN_DBA..pr_volumetria ', 
		@database_name=N'master', 
		@flags=4
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Schedule01', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20180827, 
		@active_end_date=99991231, 
		@active_start_time=53000, 
		@active_end_time=235959, 
		@schedule_uid=N'11e3bc1a-96a5-49cc-a337-3b121280e9d1'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [GRANT SELECT RTB]    Script Date: 10/6/2023 9:29:06 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/6/2023 9:29:06 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'GRANT SELECT RTB', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [GRANT SELECT RTB]    Script Date: 10/6/2023 9:29:06 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'GRANT SELECT RTB', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'Use master
GO

--CONFIG
DECLARE @UserName VARCHAR(50) = ''MODAL\ACESSO_SQL - RTB-SUSTENTACAO-PRD _SEC_''
DECLARE @PrintOnly bit = 0
DECLARE @Add_DataReader bit = 1
DECLARE @Add_DataWriter bit = 0

-------------------------------------------
DECLARE @dbname VARCHAR(50)
DECLARE @statement NVARCHAR(max)
DECLARE @ParmDefinition NVARCHAR(500);
DECLARE @UserExists bit

DECLARE @DATABASES_Fetch int
DECLARE DATABASES_CURSOR CURSOR FOR
   select distinct
        db_name(s_mf.database_id) dbName
    from
        sys.master_files s_mf
    where
        s_mf.state = 0 and -- ONLINE
        has_dbaccess(db_name(s_mf.database_id)) = 1
  and [master].sys.fn_hadr_is_primary_replica(db_name(s_mf.database_id)) = 1
  and db_name(s_mf.database_id) not in (''Master'',''tempdb'',''model'', ''MSDB'')
        and type=1
    order by 
        db_name(s_mf.database_id)
OPEN DATABASES_CURSOR
FETCH NEXT FROM DATABASES_CURSOR INTO @DBName
WHILE @@FETCH_STATUS = 0
BEGIN

--Check if user already exists in the database
SET @statement = N''USE '' + QUOTENAME(@dbname) + N'';
IF EXISTS (SELECT [name]
FROM [sys].[database_principals]
WHERE [name] = '''''' + @UserName + '''''')
set @UserExistsOUT = 1
else
set @UserExistsOUT = 0''
SET @ParmDefinition = N''@UserExistsOUT bit OUTPUT'';

EXEC sp_executesql @statement, @ParmDefinition, @UserExistsOUT = @UserExists OUTPUT

----Start Creating Script
SET @statement = ''use ''+ @dbname +'';''

--If user doesn''t exist then add CREATE statement
IF @UserExists = 0
BEGIN
SET @statement = @statement + char(10) + ''CREATE USER [''+ @UserName +''] FOR LOGIN [''+ @UserName +''];''
END

IF @Add_DataReader = 1
SET @statement = @statement + char(10) + ''EXEC sp_addrolemember N''''db_datareader'''', [''+ @UserName +''];''

IF @Add_DataWriter = 1
SET @statement = @statement + char(10) + ''EXEC sp_addrolemember N''''db_datawriter'''', [''+ @UserName +''];''

IF @PrintOnly = 1
PRINT @statement
ELSE
exec sp_executesql @statement

FETCH NEXT FROM DATABASES_CURSOR INTO @dbname
END
CLOSE DATABASES_CURSOR
DEALLOCATE DATABASES_CURSOR
', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'GRANT_SELECT_RTB', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=8, 
		@freq_subday_interval=12, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20220202, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, 
		@schedule_uid=N'dc454aa4-b583-4af0-8eff-f08e38412b01'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [IndexOptimize - USER_DATABASES]    Script Date: 10/6/2023 9:29:06 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Database Maintenance]    Script Date: 10/6/2023 9:29:06 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Database Maintenance' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Database Maintenance'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'IndexOptimize - USER_DATABASES', 
		@enabled=0, 
		@notify_level_eventlog=2, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Source: https://ola.hallengren.com', 
		@category_name=N'Database Maintenance', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [IndexOptimize - USER_DATABASES]    Script Date: 10/6/2023 9:29:06 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'IndexOptimize - USER_DATABASES', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXECUTE [dbo].[IndexOptimize]
@Databases = ''USER_DATABASES'',
@LogToTable = ''Y''', 
		@database_name=N'ADMIN_DBA', 
		@output_file_name=N'$(ESCAPE_SQUOTE(SQLLOGDIR))\$(ESCAPE_SQUOTE(JOBNAME))_$(ESCAPE_SQUOTE(STEPID))_$(ESCAPE_SQUOTE(DATE))_$(ESCAPE_SQUOTE(TIME)).txt', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [Output File Cleanup]    Script Date: 10/6/2023 9:29:06 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Database Maintenance]    Script Date: 10/6/2023 9:29:06 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Database Maintenance' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Database Maintenance'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Output File Cleanup', 
		@enabled=0, 
		@notify_level_eventlog=2, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Source: https://ola.hallengren.com', 
		@category_name=N'Database Maintenance', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Output File Cleanup]    Script Date: 10/6/2023 9:29:06 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Output File Cleanup', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'CmdExec', 
		@command=N'cmd /q /c "For /F "tokens=1 delims=" %v In (''ForFiles /P "$(ESCAPE_SQUOTE(SQLLOGDIR))" /m *_*_*_*.txt /d -30 2^>^&1'') do if EXIST "$(ESCAPE_SQUOTE(SQLLOGDIR))"\%v echo del "$(ESCAPE_SQUOTE(SQLLOGDIR))"\%v& del "$(ESCAPE_SQUOTE(SQLLOGDIR))"\%v"', 
		@output_file_name=N'$(ESCAPE_SQUOTE(SQLLOGDIR))\$(ESCAPE_SQUOTE(JOBNAME))_$(ESCAPE_SQUOTE(STEPID))_$(ESCAPE_SQUOTE(DATE))_$(ESCAPE_SQUOTE(TIME)).txt', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [Particionamento-AB_PAGAMENTO_INSTANTANEO]    Script Date: 10/6/2023 9:29:06 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/6/2023 9:29:06 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Particionamento-AB_PAGAMENTO_INSTANTANEO', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Job solicitado pelo Autbank, para particionamento de algumas tabelas.

https://glpi.modal.net.br/index.html/front/ticket.form.php?id=143845
', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', 
		@notify_email_operator_name=N'Alerta', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Step_01]    Script Date: 10/6/2023 9:29:06 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Step_01', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'use master
go

declare @origin_db varchar(30) = ''AB_PAGAMENTO_INSTANTANEO''

IF [master].sys.fn_hadr_is_primary_replica(@origin_db) = 1
BEGIN
 EXEC AB_PAGAMENTO_INSTANTANEO.DBO.P_SPLIT
END


', 
		@database_name=N'master', 
		@output_file_name=N'E:\MSSQL\SQLAgent_Logging\Particionamento_Autbank_Pagamento_instantaneo.log', 
		@flags=4
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Sched_01', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20201030, 
		@active_end_date=99991231, 
		@active_start_time=30000, 
		@active_end_time=235959, 
		@schedule_uid=N'04fa194a-460b-45bb-ac01-a7bcee7ae08b'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [Particionamento-DBSMART_LOGSIS..TMas_Log_Sistema]    Script Date: 10/6/2023 9:29:06 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Database Maintenance]    Script Date: 10/6/2023 9:29:06 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Database Maintenance' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Database Maintenance'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Particionamento-DBSMART_LOGSIS..TMas_Log_Sistema', 
		@enabled=0, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Job para particionamento mensal autom�tico', 
		@category_name=N'Database Maintenance', 
		@owner_login_name=N'sa', 
		@notify_email_operator_name=N'Alerta', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Particionamento mensal automatico]    Script Date: 10/6/2023 9:29:06 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Particionamento mensal automatico', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'/* Prerequisites

--Considering the creation at month 2010-12...

Filegroups: [FG_Previus], [FG_part_1900-01], [FG_part_2010-09], [FG_part_2010-10], [FG_part_2010-11], [FG_Next]

CREATE PARTITION FUNCTION PartFunction_Dt_LogSistema (datetime)
AS RANGE RIGHT FOR VALUES (''1900-01-01'',''2010-10-01'', ''2010-11-01'', ''2010-12-01'', ''2050-01-01'')
GO

CREATE PARTITION SCHEME PartSchema_Dt_LogSistema
AS PARTITION PartFunction_Dt_LogSistema
TO ([FG_Previus], [FG_part_1900-01], [FG_part_2010-09], [FG_part_2010-10], [FG_part_2010-11], [FG_Next])
GO

*/

--
/* Removing old FG and Partition */
--

Use [DBSMART_LOGSIS]
GO

declare @origin_db varchar(30) = ''DBSMART_LOGSIS''

IF [master].sys.fn_hadr_is_primary_replica(@origin_db) = 1
BEGIN

 /* Global Variables */
 --declare @today as datetime2 = ''2019-08-01''
 declare @today as datetime2 = getdate()
 declare @MonthsAgoForRemoving as int = 2
 declare @DatabaseName as varchar(50) = ''DBSMART_LOGSIS''
 declare @TableName as varchar(50) = ''dbo.TMas_LogSistema''
 declare @PartitionSchema as varchar(50) = ''PartSchema_Dt_LogSistema''
 declare @PartitionFunction as varchar(50) = ''PartFunction_Dt_LogSistema()''
 declare @FGNamePrefix as varchar(10) = ''FG_part_''
 declare @DatafilePath as varchar(1024) = ''E:\MSSQL\DATA\''

 /* Month to Remove */
 declare @MonthToRemove varchar(7)
 declare @FGMonthToRemove varchar(7)
 declare @PartitionBoundary as varchar(10) = ''-01''
 select @MonthToRemove = format(dateadd(mm, -@MonthsAgoForRemoving, @today), ''yyyy-MM'')
 select @FGMonthToRemove = format(dateadd(mm, -@MonthsAgoForRemoving-1, @today), ''yyyy-MM'')
 declare @FgToRemove as varchar(50)
 SET @FgToRemove = @FGNamePrefix + @FGMonthToRemove;
 SET @PartitionBoundary = @MonthToRemove + @PartitionBoundary;

 PRINT CHAR(13) + '' => '' + convert(varchar, getdate(), 121) + '' - Starting partition maintenance for database: '' + @DatabaseName + '', table: '' + @TableName + CHAR(13)

 PRINT '' => '' + convert(varchar, getdate(), 121) + '' - Truncating partition [2] with Boundary: '' + @PartitionBoundary
 PRINT ''  * Lines older than '' + @PartitionBoundary + '' are going to be removed. Criteria: where date < '' + @PartitionBoundary
 --Always the second partition is truncated, consider this for especifing the number of partition and the @MonthsAgoForRemoving variable
 declare @TruncatePartition varchar(100) = ''TRUNCATE TABLE '' + @TableName + '' WITH (PARTITIONS(2));''
 exec (@TruncatePartition)

 PRINT ''  - Command: ALTER PARTITION FUNCTION '' + @PartitionFunction + '' MERGE RANGE ('' + '''''''' + @PartitionBoundary + '''''''' + '');'' + CHAR(13)
 declare @AlterPartFunctionMerge varchar(100) = ''ALTER PARTITION FUNCTION '' + @PartitionFunction + '' MERGE RANGE ('' + '''''''' + @PartitionBoundary + '''''''' + '');''
 exec (@AlterPartFunctionMerge)

 PRINT '' => '' + convert(varchar, getdate(), 121) + '' - Removing old FileGroup and it''''s files: ''
 if exists (select * from sys.filegroups where name = @FgToRemove)
 BEGIN
  declare @RemoveFile as VARCHAR(256) = ''ALTER DATABASE '' + @DatabaseName + '' REMOVE FILE ['' + @DatabaseName + ''_part_'' + @FGMonthToRemove + '']''
  exec (@RemoveFile)

  declare @RemoveFG as VARCHAR(256) = ''ALTER DATABASE '' + @DatabaseName + '' REMOVE FILEGROUP ['' + @FgToRemove + '']''
  exec (@RemoveFG)
 END
 ELSE
  PRINT ''  * WARNING * ['' + @FgToRemove + '']'' + '' does not exist!''
   
 --
 /* Creating Next FG and Partition */
 --

 /* Next Month */
 declare @NextMonth as varchar(7)
 declare @FirstDayOfNextMonth as varchar(10) = ''-01''
 select @NextMonth = format(dateadd(mm, +1, @today), ''yyyy-MM'');
 SET @FirstDayOfNextMonth = @NextMonth + @FirstDayOfNextMonth;

 PRINT CHAR(13) + '' => '' + convert(varchar, getdate(), 121) + '' - Creating new FileGroup ['' + @FGNamePrefix + @NextMonth + '']''
 declare @CreateFGNext as VARCHAR(256) = ''ALTER DATABASE '' + @DatabaseName + '' ADD FILEGROUP ['' + @FGNamePrefix + @NextMonth + '']''
 EXEC (@CreateFGNext)

 PRINT '' => '' + convert(varchar, getdate(), 121) + '' - Adding File "'' + @DatafilePath + @DatabaseName + ''_part_'' + @NextMonth + ''.ndf" to Filegroup ['' + @FGNamePrefix + @NextMonth + '']'' + CHAR(13)
 declare @CreateFGDataFile as VARCHAR(2048) = ''ALTER DATABASE ['' + @DatabaseName + ''] '' +
 ''ADD FILE ( NAME = N'''''' + @DatabaseName + ''_part_'' + @NextMonth + '''''', '' +
 ''FILENAME = N'''''' + @DatafilePath + @DatabaseName + ''_part_'' + @NextMonth + ''.ndf'''', '' +
 ''SIZE = 65536KB , MAXSIZE = UNLIMITED , FILEGROWTH = 262144KB ) TO FILEGROUP ['' + @FGNamePrefix + @NextMonth + '']'' 
 EXEC (@CreateFGDataFile)

 PRINT '' => '' + convert(varchar, getdate(), 121) + '' - Creating new partition range: ''
 PRINT '' - Command: ALTER PARTITION SCHEME '' + @PartitionSchema + '' NEXT USED ['' + @FGNamePrefix + @NextMonth + ''];''
 declare @AlterPartSchemaNextUsed varchar(100) = ''ALTER PARTITION SCHEME '' + @PartitionSchema + '' NEXT USED ['' + @FGNamePrefix + @NextMonth + ''];''
 exec (@AlterPartSchemaNextUsed)

 PRINT '' - Command: ALTER PARTITION FUNCTION '' + @PartitionFunction + '' SPLIT RANGE ('' + '''''''' + @FirstDayOfNextMonth + '''''''' + '');''
 declare @AlterPartFunctionSplit varchar(100) = ''ALTER PARTITION FUNCTION '' + @PartitionFunction + '' SPLIT RANGE ('' + '''''''' + @FirstDayOfNextMonth + '''''''' + '');''
 exec (@AlterPartFunctionSplit)

 PRINT CHAR(13) + '' => '' + convert(varchar, getdate(), 121) + '' - Partition maintenance ended!''

END', 
		@database_name=N'master', 
		@output_file_name=N'E:\MSSQL\SQLAgent_Logging\Particionamento_DBSMART_LOGSIS_TMas_LogSistema_$(ESCAPE_SQUOTE(DATE))_$(ESCAPE_SQUOTE(TIME)).log', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Particionamento autom�tico', 
		@enabled=1, 
		@freq_type=32, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=2, 
		@freq_recurrence_factor=1, 
		@active_start_date=20190429, 
		@active_end_date=99991231, 
		@active_start_time=170000, 
		@active_end_time=235959, 
		@schedule_uid=N'8fceba1c-0bab-433c-a9cd-f9285cfcd99c'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [Particionamento-DBSMART_MASTER..TMas_Log_Sistema]    Script Date: 10/6/2023 9:29:07 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Database Maintenance]    Script Date: 10/6/2023 9:29:07 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Database Maintenance' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Database Maintenance'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Particionamento-DBSMART_MASTER..TMas_Log_Sistema', 
		@enabled=0, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Job para particionamento mensal autom�tico', 
		@category_name=N'Database Maintenance', 
		@owner_login_name=N'sa', 
		@notify_email_operator_name=N'Alerta', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Particionamento mensal automatico]    Script Date: 10/6/2023 9:29:07 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Particionamento mensal automatico', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'/* Prerequisites

--Considering the creation at month 2010-12...

Filegroups: [FG_Previus], [FG_part_1900-01], [FG_part_2010-09], [FG_part_2010-10], [FG_part_2010-11], [FG_Next]

CREATE PARTITION FUNCTION PartFunction_Dt_LogSistema (datetime)
AS RANGE RIGHT FOR VALUES (''1900-01-01'',''2010-10-01'', ''2010-11-01'', ''2010-12-01'', ''2050-01-01'')
GO

CREATE PARTITION SCHEME PartSchema_Dt_LogSistema
AS PARTITION PartFunction_Dt_LogSistema
TO ([FG_Previus], [FG_part_1900-01], [FG_part_2010-09], [FG_part_2010-10], [FG_part_2010-11], [FG_Next])
GO

*/

--
/* Removing old FG and Partition */
--

Use [DBSMART_MASTER]
GO

declare @origin_db varchar(30) = ''DBSMART_MASTER''

IF [master].sys.fn_hadr_is_primary_replica(@origin_db) = 1
BEGIN

 /* Global Variables */
 --declare @today as datetime2 = ''2019-08-01''
 declare @today as datetime2 = getdate()
 declare @MonthsAgoForRemoving as int = 2
 declare @DatabaseName as varchar(50) = ''DBSMART_MASTER''
 declare @TableName as varchar(50) = ''dbo.TMas_LogSistema''
 declare @PartitionSchema as varchar(50) = ''PartSchema_Dt_LogSistema''
 declare @PartitionFunction as varchar(50) = ''PartFunction_Dt_LogSistema()''
 declare @FGNamePrefix as varchar(10) = ''FG_part_''
 declare @DatafilePath as varchar(1024) = ''E:\MSSQL\DATA\''

 /* Month to Remove */
 declare @MonthToRemove varchar(7)
 declare @FGMonthToRemove varchar(7)
 declare @PartitionBoundary as varchar(10) = ''-01''
 select @MonthToRemove = format(dateadd(mm, -@MonthsAgoForRemoving, @today), ''yyyy-MM'')
 select @FGMonthToRemove = format(dateadd(mm, -@MonthsAgoForRemoving-1, @today), ''yyyy-MM'')
 declare @FgToRemove as varchar(50)
 SET @FgToRemove = @FGNamePrefix + @FGMonthToRemove;
 SET @PartitionBoundary = @MonthToRemove + @PartitionBoundary;

 PRINT CHAR(13) + '' => '' + convert(varchar, getdate(), 121) + '' - Starting partition maintenance for database: '' + @DatabaseName + '', table: '' + @TableName + CHAR(13)

 PRINT '' => '' + convert(varchar, getdate(), 121) + '' - Truncating partition [2] with Boundary: '' + @PartitionBoundary
 PRINT ''  * Lines older than '' + @PartitionBoundary + '' are going to be removed. Criteria: where date < '' + @PartitionBoundary
 --Always the second partition is truncated, consider this for especifing the number of partition and the @MonthsAgoForRemoving variable
 declare @TruncatePartition varchar(100) = ''TRUNCATE TABLE '' + @TableName + '' WITH (PARTITIONS(2));''
 exec (@TruncatePartition)

 PRINT ''  - Command: ALTER PARTITION FUNCTION '' + @PartitionFunction + '' MERGE RANGE ('' + '''''''' + @PartitionBoundary + '''''''' + '');'' + CHAR(13)
 declare @AlterPartFunctionMerge varchar(100) = ''ALTER PARTITION FUNCTION '' + @PartitionFunction + '' MERGE RANGE ('' + '''''''' + @PartitionBoundary + '''''''' + '');''
 exec (@AlterPartFunctionMerge)

 PRINT '' => '' + convert(varchar, getdate(), 121) + '' - Removing old FileGroup and it''''s files: ''
 if exists (select * from sys.filegroups where name = @FgToRemove)
 BEGIN
  declare @RemoveFile as VARCHAR(256) = ''ALTER DATABASE '' + @DatabaseName + '' REMOVE FILE ['' + @DatabaseName + ''_part_'' + @FGMonthToRemove + '']''
  exec (@RemoveFile)

  declare @RemoveFG as VARCHAR(256) = ''ALTER DATABASE '' + @DatabaseName + '' REMOVE FILEGROUP ['' + @FgToRemove + '']''
  exec (@RemoveFG)
 END
 ELSE
  PRINT ''  * WARNING * ['' + @FgToRemove + '']'' + '' does not exist!''
   
 --
 /* Creating Next FG and Partition */
 --

 /* Next Month */
 declare @NextMonth as varchar(7)
 declare @FirstDayOfNextMonth as varchar(10) = ''-01''
 select @NextMonth = format(dateadd(mm, +1, @today), ''yyyy-MM'');
 SET @FirstDayOfNextMonth = @NextMonth + @FirstDayOfNextMonth;

 PRINT CHAR(13) + '' => '' + convert(varchar, getdate(), 121) + '' - Creating new FileGroup ['' + @FGNamePrefix + @NextMonth + '']''
 declare @CreateFGNext as VARCHAR(256) = ''ALTER DATABASE '' + @DatabaseName + '' ADD FILEGROUP ['' + @FGNamePrefix + @NextMonth + '']''
 EXEC (@CreateFGNext)

 PRINT '' => '' + convert(varchar, getdate(), 121) + '' - Adding File "'' + @DatafilePath + @DatabaseName + ''_part_'' + @NextMonth + ''.ndf" to Filegroup ['' + @FGNamePrefix + @NextMonth + '']'' + CHAR(13)
 declare @CreateFGDataFile as VARCHAR(2048) = ''ALTER DATABASE ['' + @DatabaseName + ''] '' +
 ''ADD FILE ( NAME = N'''''' + @DatabaseName + ''_part_'' + @NextMonth + '''''', '' +
 ''FILENAME = N'''''' + @DatafilePath + @DatabaseName + ''_part_'' + @NextMonth + ''.ndf'''', '' +
 ''SIZE = 65536KB , MAXSIZE = UNLIMITED , FILEGROWTH = 262144KB ) TO FILEGROUP ['' + @FGNamePrefix + @NextMonth + '']'' 
 EXEC (@CreateFGDataFile)

 PRINT '' => '' + convert(varchar, getdate(), 121) + '' - Creating new partition range: ''
 PRINT '' - Command: ALTER PARTITION SCHEME '' + @PartitionSchema + '' NEXT USED ['' + @FGNamePrefix + @NextMonth + ''];''
 declare @AlterPartSchemaNextUsed varchar(100) = ''ALTER PARTITION SCHEME '' + @PartitionSchema + '' NEXT USED ['' + @FGNamePrefix + @NextMonth + ''];''
 exec (@AlterPartSchemaNextUsed)

 PRINT '' - Command: ALTER PARTITION FUNCTION '' + @PartitionFunction + '' SPLIT RANGE ('' + '''''''' + @FirstDayOfNextMonth + '''''''' + '');''
 declare @AlterPartFunctionSplit varchar(100) = ''ALTER PARTITION FUNCTION '' + @PartitionFunction + '' SPLIT RANGE ('' + '''''''' + @FirstDayOfNextMonth + '''''''' + '');''
 exec (@AlterPartFunctionSplit)

 PRINT CHAR(13) + '' => '' + convert(varchar, getdate(), 121) + '' - Partition maintenance ended!''

END', 
		@database_name=N'master', 
		@output_file_name=N'E:\MSSQL\SQLAgent_Logging\Particionamento_DBSMART_MASTER_TMas_LogSistema_$(ESCAPE_SQUOTE(DATE))_$(ESCAPE_SQUOTE(TIME)).log', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Particionamento autom�tico', 
		@enabled=1, 
		@freq_type=32, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=2, 
		@freq_recurrence_factor=1, 
		@active_start_date=20190429, 
		@active_end_date=99991231, 
		@active_start_time=170000, 
		@active_end_time=235959, 
		@schedule_uid=N'8fceba1c-0bab-433c-a9cd-f9285cfcd99c'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [Shrink T-Log]    Script Date: 10/6/2023 9:29:07 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Database Maintenance]    Script Date: 10/6/2023 9:29:07 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Database Maintenance' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Database Maintenance'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Shrink T-Log', 
		@enabled=0, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Shrink T-Log', 
		@category_name=N'Database Maintenance', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Shrink T-Log]    Script Date: 10/6/2023 9:29:07 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Shrink T-Log', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @DBName varchar(255)
DECLARE @LogName varchar(255)
DECLARE @DATABASES_Fetch int
DECLARE DATABASES_CURSOR CURSOR FOR
   select distinct
        name, db_name(s_mf.database_id) dbName
    from
        sys.master_files s_mf
    where
        s_mf.state = 0 and -- ONLINE
        has_dbaccess(db_name(s_mf.database_id)) = 1
  and [master].sys.fn_hadr_is_primary_replica(db_name(s_mf.database_id)) = 1
  and db_name(s_mf.database_id) not in (''Master'',''tempdb'',''model'')
    and db_name(s_mf.database_id) not like ''MSDB%''
    and db_name(s_mf.database_id) not like ''Report%''
    and type=1
    order by 
        db_name(s_mf.database_id)
OPEN DATABASES_CURSOR
FETCH NEXT FROM DATABASES_CURSOR INTO @LogName, @DBName
WHILE @@FETCH_STATUS = 0
BEGIN
 exec (''USE ['' + @DBName + ''] ; DBCC SHRINKFILE (N'''''' + @LogName + '''''' , 0, TRUNCATEONLY)'')
 FETCH NEXT FROM DATABASES_CURSOR INTO @LogName, @DBName
END
CLOSE DATABASES_CURSOR
DEALLOCATE DATABASES_CURSOR', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Shrink T-Log', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=4, 
		@freq_subday_interval=5, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20210703, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, 
		@schedule_uid=N'bea5ca9c-77b6-47a4-be65-2f7b9f41cf84'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [sp_delete_backuphistory]    Script Date: 10/6/2023 9:29:07 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Database Maintenance]    Script Date: 10/6/2023 9:29:07 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Database Maintenance' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Database Maintenance'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'sp_delete_backuphistory', 
		@enabled=0, 
		@notify_level_eventlog=2, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Source: https://ola.hallengren.com', 
		@category_name=N'Database Maintenance', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [sp_delete_backuphistory]    Script Date: 10/6/2023 9:29:07 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'sp_delete_backuphistory', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @CleanupDate datetime
SET @CleanupDate = DATEADD(dd,-30,GETDATE())
EXECUTE dbo.sp_delete_backuphistory @oldest_date = @CleanupDate', 
		@database_name=N'msdb', 
		@output_file_name=N'$(ESCAPE_SQUOTE(SQLLOGDIR))\$(ESCAPE_SQUOTE(JOBNAME))_$(ESCAPE_SQUOTE(STEPID))_$(ESCAPE_SQUOTE(DATE))_$(ESCAPE_SQUOTE(TIME)).txt', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [sp_purge_jobhistory]    Script Date: 10/6/2023 9:29:07 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Database Maintenance]    Script Date: 10/6/2023 9:29:07 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Database Maintenance' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Database Maintenance'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'sp_purge_jobhistory', 
		@enabled=0, 
		@notify_level_eventlog=2, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Source: https://ola.hallengren.com', 
		@category_name=N'Database Maintenance', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [sp_purge_jobhistory]    Script Date: 10/6/2023 9:29:07 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'sp_purge_jobhistory', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @CleanupDate datetime
SET @CleanupDate = DATEADD(dd,-30,GETDATE())
EXECUTE dbo.sp_purge_jobhistory @oldest_date = @CleanupDate', 
		@database_name=N'msdb', 
		@output_file_name=N'$(ESCAPE_SQUOTE(SQLLOGDIR))\$(ESCAPE_SQUOTE(JOBNAME))_$(ESCAPE_SQUOTE(STEPID))_$(ESCAPE_SQUOTE(DATE))_$(ESCAPE_SQUOTE(TIME)).txt', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [syspolicy_purge_history]    Script Date: 10/6/2023 9:29:07 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/6/2023 9:29:07 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'syspolicy_purge_history', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', 
		@notify_email_operator_name=N'Alerta', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Verify that automation is enabled.]    Script Date: 10/6/2023 9:29:07 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Verify that automation is enabled.', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=1, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'IF (msdb.dbo.fn_syspolicy_is_automation_enabled() != 1)
        BEGIN
            RAISERROR(34022, 16, 1)
        END', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Purge history.]    Script Date: 10/6/2023 9:29:07 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Purge history.', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC msdb.dbo.sp_syspolicy_purge_history', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Erase Phantom System Health Records.]    Script Date: 10/6/2023 9:29:07 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Erase Phantom System Health Records.', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'PowerShell', 
		@command=N'if (''$(ESCAPE_SQUOTE(INST))'' -eq ''MSSQLSERVER'') {$a = ''\DEFAULT''} ELSE {$a = ''''};
(Get-Item SQLSERVER:\SQLPolicy\$(ESCAPE_NONE(SRVR))$a).EraseSystemHealthPhantomRecords()', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'syspolicy_purge_history_schedule', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20080101, 
		@active_end_date=99991231, 
		@active_start_time=20000, 
		@active_end_time=235959, 
		@schedule_uid=N'ba9b5a69-f383-4b13-8ffb-28a5132393f6'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

/****** Object:  Job [Verifica Atraso AG]    Script Date: 10/6/2023 9:29:07 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/6/2023 9:29:07 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Verifica Atraso AG', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', 
		@notify_email_operator_name=N'Alerta', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [exec pr_verif_atraso_ag]    Script Date: 10/6/2023 9:29:07 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'exec pr_verif_atraso_ag', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'exec ADMIN_DBA.[dbo].[pr_verif_atraso_ag]', 
		@database_name=N'master', 
		@flags=4
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Verif_Atraso_Ag - Dia', 
		@enabled=1, 
		@freq_type=8, 
		@freq_interval=62, 
		@freq_subday_type=4, 
		@freq_subday_interval=20, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20190610, 
		@active_end_date=99991231, 
		@active_start_time=52000, 
		@active_end_time=194000, 
		@schedule_uid=N'9096a500-6c76-4fe5-b2f0-7174597c0dc8'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Verif_Atraso_Ag - Fim de Semana', 
		@enabled=1, 
		@freq_type=8, 
		@freq_interval=65, 
		@freq_subday_type=8, 
		@freq_subday_interval=1, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20190613, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, 
		@schedule_uid=N'347bda6e-cab2-407d-a13b-22661c753f31'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Verif_Atraso_Ag - Noite', 
		@enabled=1, 
		@freq_type=8, 
		@freq_interval=62, 
		@freq_subday_type=4, 
		@freq_subday_interval=30, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20190613, 
		@active_end_date=99991231, 
		@active_start_time=200000, 
		@active_end_time=50000, 
		@schedule_uid=N'20522960-2b7c-4c62-abb4-e0895b863dca'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO


