#!/bin/bash

for FS in `df -h | grep -Ev 'Mounted' | awk '{ print $6 }'`
do
   LV="`df -h $FS | grep -v 'Use%' | awk '{ print $1 }' | cut -f 4 -d '/' | cut -f 2 -d -`"
   BLSIZE="`df -h $FS | grep -v 'Use%' | awk '{ print $2 }'`"
   USED="`df -h $FS | grep -v 'Use%' | awk '{ print $3 }'`"
   FREE="`df -h $FS | grep -v 'Use%' | awk '{ print $4 }'`"
   PUSED="`df -h $FS | grep -v 'Use%' | awk '{ print $5 }'`"
   MOUNT="$FS"


printf "%-9s %-4s %-4s %-4s %-4s %-13s  \n" "$MOUNT""|""$BLSIZE""|""$USED""|""$FREE""|""$PUSED"
done


